cd src
icmbuild
cd ..

mkdir -p data
cd data
wget http://itu.dk/~robv/data/monoise/en.tar.gz
tar -zxvf en.tar.gz
wget http://itu.dk/~robv/data/monoise/id.tar.gz
tar -zxvf id.tar.gz
wget http://itu.dk/~robv/data/monoise/tr.tar.gz
tar -zxvf tr.tar.gz
wget http://itu.dk/~robv/data/monoise/de.tar.gz
tar -zxvf de.tar.gz
rm -rf *tar.gz

cp -r ../TrDeNormalisation ../ .

mkdir -p iden
mkdir -p iden/langFeat
python3 ../scripts/convID.py TrDeNormalisation/data/id-en/825_Indonesian_English_CodeMixed.csv > iden/langFeat/all
python3 ../scripts/split.py iden/langFeat/all .8 iden/langFeat/train iden/langFeat/test
mkdir -p iden/norm
cut -f 2-99 iden/langFeat/train > iden/norm/train
cut -f 2-99 iden/langFeat/test > iden/norm/test
python3 ../scripts/kfold.py 10 iden/langFeat/train iden/langFeat/10fold
python3 ../scripts/kfold.py 10 iden/norm/train iden/norm/10fold


mkdir -p trde
mkdir -p trde/fineGrained/
python3 ../scripts/algn2norm.py TrDeNormalisation/data/whole/trde_twitter.whole.simp_orig_norm.giza_man.algn.lid > trde/fineGrained/all
mkdir -p trde/langFeat
cp trde/fineGrained/all trde/langFeat/all
python3 ../scripts/0.prep.convLangs.py trde/langFeat/all
python3 ../scripts/split.py trde/langFeat/all .8 trde/langFeat/train trde/langFeat/test
python3 ../scripts/split.py trde/fineGrained/all .8 trde/fineGrained/train trde/langFeat/test
mkdir -p trde/norm
cut -f 2-99 trde/langFeat/train > trde/norm/train
cut -f 2-99 trde/langFeat/test > trde/norm/test
python3 ../scripts/kfold.py 10 trde/langFeat/train trde/langFeat/10fold
python3 ../scripts/kfold.py 10 trde/fineGrained/train trde/fineGrained/10fold
python3 ../scripts/kfold.py 10 trde/norm/train trde/norm/10fold
cd ..

mkdir -p working
git clone https://bitbucket.org/robvanderg/monoise.git

cd monoise/src
git reset --hard 082f691
icmbuild
cd ../../

mkdir -p preds

