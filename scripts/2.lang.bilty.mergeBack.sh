cat bilstm-aux/trde.*.task0 | cut -f 3 > bilty/trde.pred
cat bilstm-aux/iden.*.task0 | cut -f 3 > bilty/iden.pred

mkdir -p data/trde/predicted-bilty/
mkdir -p data/iden/predicted-bilty/
paste bilty/trde.pred data/trde/norm/train > data/trde/predicted-bilty/train
paste bilty/iden.pred data/iden/norm/train > data/iden/predicted-bilty/train
python3 scripts/kfold.py 10 data/trde/predicted-bilty/train data/trde/predicted-bilty/10fold
python3 scripts/kfold.py 10 data/iden/predicted-bilty/train data/iden/predicted-bilty/10fold

