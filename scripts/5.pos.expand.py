import sys
import os

posDir = 'data/trde/pos/'
for posFile in os.listdir(posDir):
    if 'txt' in posFile:
        continue
    devFile = posDir + posFile

    newData = []
    prev = 0
    for line in open(devFile):
        tok = line[:-1].split('\t')
        if tok == ['']:
            newData.append('')
        elif ' ' in tok[0]:
            toktok = tok[0].split(' ')
            for i in range(len(toktok)):
                newData.append(toktok[i] + '\tNOTAG')# + '\t' + 'NOTAG-' + tok[1])
            #newData.append(toktok[-1] + '\t' + tok[1])
        elif tok[0] == '':
            prev += 1
        else:
            newData.append(tok[0] + '\t' + tok[-1])

    outFile = open(devFile + '.txt', 'w')
    for line in newData:
        outFile.write(line + '\n')
    outFile.close()
    
        
