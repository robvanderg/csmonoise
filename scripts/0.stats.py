import os
import utils

trRaw, trGold, trLang = utils.loadNormData('data/trde/langFeat/train', False)
idRaw, idGold, idLang = utils.loadNormData('data/iden/langFeat/train', True)

def getStats(langData, rawData, goldData, vocab):
    words = 0
    changed = 0
    split = 0
    merge = 0
    oov = 0
    csRatios = []
    totalLang1 = 0
    totalLang2 = 0
    totalUnd = 0
    for langSent, rawSent, goldSent in zip(langData, rawData, goldData):
        wordLang1 = 0
        wordLang2 = 0
        und = 0
        for langWord, rawWord, goldWord in zip(langSent, rawSent, goldSent):
            words += 1
            if rawWord != goldWord:
                changed += 1
            if ' ' in goldWord.strip():
                split += 1
            if goldWord == '':
                merge += 1
            if rawWord not in vocab:
                oov += 1
            if langWord in ['tr', 'id']:
                wordLang1 += 1
                totalLang1 += 1
            elif langWord in ['de', 'en']:
                wordLang2 += 1
                totalLang2 += 1
            else:
                totalUnd += 1
        csRatios.append(100 * (1- (max(wordLang1, wordLang2)/(wordLang1 + wordLang2))))
    csRatio = 0
    for ratio in csRatios:
        csRatio += ratio
    csRatio = csRatio/len(langData)
    total = totalLang1 + totalLang2 + totalUnd
    lang1ratio = (totalLang1 /  total) * 100
    lang2ratio = (totalLang2 /  total) * 100
    undRatio = (totalUnd /  total) * 100
    return '{:,d}'.format(words), '{:.2f}'.format(changed/words*100), '{:.2f}'.format(split/words*100), '{:.2f}'.format(merge/words*100), '{:.2f}'.format(lang1ratio), '{:.2f}'.format(lang2ratio), '{:.2f}'.format(undRatio), '{:.2f}'.format(csRatio)

trdeDict = set()
for line in open('data/tr/aspell'):
    trdeDict.add(line.strip())
for line in open('data/de/aspell'):
    trdeDict.add(line.strip())

idenDict = set()
for line in open('data/id/aspell'):
    idenDict.add(line.strip())
for line in open('data/en/aspell'):
    idenDict.add(line.strip())

print('Tr-De &  ' + ' & '.join(getStats(trLang, trRaw, trGold, trdeDict)) + ' \\\\')
print('Id-En & ' + ' & '.join(getStats(idLang, idRaw, idGold, idenDict)) + ' \\\\')


