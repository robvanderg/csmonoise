import os
import imp
signif = imp.load_source('scripts/bootstrapSign.py', 'scripts/bootstrapSign.py')


norms = ['raw', 'multiling', 'langaware', 'gold']
poss = ['marmot', 'bilty', 'bert']

def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

def readPos(path):
    data = []
    curSent = []
    for line in open(path):
        tok = line[:-1].split('\t')
        if len(tok) < 2:
            data.append(curSent)
            curSent = []
        else:
            if tok[-1] == 'CONJ':
                tok[-1] = 'CCONJ'
            curSent.append(tok)
    return data

def eval(goldPath, predPath):
    total = 0
    cor = 0
    sentScores = []
    golds = readPos(goldPath)
    preds = readPos(predPath)
    for goldSent, predSent in zip(golds, preds):
        predTags = []
        predIdx = 0
        for goldWordIdx, goldWord in enumerate(goldSent):
            if ' ' in goldWord[0]:
                count = goldWord[0].count(' ')
                tags = []
                for i in range(count +1):
                    tags.append(predSent[predIdx+i][-1])
                predTags.append(',,'.join(tags))
                predIdx += count + 1
            elif goldWord[0] == '':
                if predIdx < len(predSent):
                    predTags.append("NEXT-" + predSent[predIdx][-1])
                else:
                    predTags.append("NEXT-" + predSent[-1][-1])
            else:
                predTags.append(predSent[predIdx][-1])
                predIdx += 1

        mergeOptions = []
        sentCor = 0
        sentTotal = 0
        for predTag, goldTag in zip(predTags, goldSent):
            goldWord = goldTag[0]
            goldTag = goldTag[-1]
            predTagOptions = predTag.split(',,')
            mergeOptions.append(goldTag)
            if predTag.startswith('NEXT'):
                continue
            else:
                #if goldWord[0] == '@':
                #    continue
                if len(intersection(mergeOptions, predTagOptions)) > 0:
                    cor += 1
                    sentCor += 1
                sentTotal += 1
                mergeOptions = []
                total += 1
        sentScores.append(sentCor/sentTotal)
    return sentScores, cor/total

if __name__ == '__main__':
    print(' & '.join([''] + norms ) + ' \\\\')
    for pos in poss:
        prevScores = []
        row = [pos]
        for norm in norms:
            pred = 'posPreds/' + pos + '.' + norm + '.out'
            if pos == 'bilty':
                pred += '.task0'
            gold = 'data/trde/pos/' + norm + '.dev'
            sentScores, score = eval(gold, pred)
            row.append('{:.2f}'.format(score*100))
            if prevScores != []:
                if signif.getP([prevScores, sentScores]) < .05:
                    row[-1] = '$^*$' + row[-1]
            prevScores = sentScores
        print(' & '.join(row) + ' \\\\') 

