for dataset in ['trde', 'iden']:
    for fold in range(10):
        fold = str(fold)
        modelPath = dataset +'.' + fold
        trainPath = '../bilty/' + dataset + '.kfold.train.' + fold
        devPath = '../bilty/' + dataset + '.kfold.dev.' + fold
        cmd = 'cd bilstm-aux && python3 src/structbilty.py --model ' + modelPath + ' --train ' + trainPath
        cmd += ' --dev ' + devPath + ' --test ' + devPath + ' --output ' + modelPath
        cmd += ' --embeds ../polyglot.' + dataset + '.txt && cd ../'
        print(cmd)

