from pprint import pprint
import os

if not os.path.isdir('config'):
    os.mkdir('config')

datasets = ['trde', 'iden']
for dataset in datasets:
    data = {dataset: {'word_idx':1, 'tasks': {'langId': {'column_idx':0, 'task_type': 'seq'}}}}
    for fold in range(10):
        data[dataset]['train_data_path'] = '../data/' + dataset + '/langFeat/10fold.train.' + str(fold)
        data[dataset]['validation_data_path'] = '../data/' + dataset + '/langFeat/10fold.dev.' + str(fold)
        data[dataset]['test_data_path'] = '../data/' + dataset + '/langFeat/10fold.dev.' + str(fold)
        data[dataset]['copy_other_columns'] = True
        jsonPath = 'config/' + dataset + '.' + str(fold) + '.json'
        with open(jsonPath, 'wt') as out:
            pprint(data, stream=out)
        os.system( "sed -i \"s;True;true;g\" " + jsonPath)
        cmd = 'cd mtp && python3 train.py --parameters_config configs/params.json --dataset_config ../' + jsonPath + ' --name ' + dataset + '.' + str(fold) + ' --device 0 && cd ../'
        print(cmd)


