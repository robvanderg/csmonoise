git clone https://github.com/facebookresearch/MUSE.git
cd MUSE/
sed -i "s;self.word_translation;#self.word_translation;g" src/evaluation/evaluator.py

wget http://bit.ly/19bSoAS -O polyglot-en.pkl
wget http://bit.ly/19bSoRk -O polyglot-de.pkl
wget http://bit.ly/19bSjgz -O polyglot-id.pkl
wget http://bit.ly/12FJeW0 -O polyglot-tr.pkl

python2 ../scripts/0.polyglot2txt.py polyglot-en.pkl > polyglot-en.txt
python2 ../scripts/0.polyglot2txt.py polyglot-id.pkl > polyglot-id.txt
python2 ../scripts/0.polyglot2txt.py polyglot-tr.pkl > polyglot-tr.txt
python2 ../scripts/0.polyglot2txt.py polyglot-de.pkl > polyglot-de.txt

python3 unsupervised.py --src_lang en --tgt_lang id --src_emb polyglot-en.txt --tgt_emb polyglot-id.txt --n_refinement 5  --emb_dim 64 --export txt --exp_path . --exp_name polyglot-iden
python3 unsupervised.py --src_lang de --tgt_lang tr --src_emb polyglot-de.txt --tgt_emb polyglot-tr.txt --n_refinement 5  --emb_dim 64 --export txt --exp_path . --exp_name polyglot-trde

cd ../

python3 scripts/0.mergeEmbeds.py MUSE/polyglot-trde/*/vectors-tr.txt MUSE/polyglot-trde/*/vectors-de.txt polyglot.trde.txt
python3 scripts/0.mergeEmbeds.py MUSE/polyglot-iden/*/vectors-id.txt MUSE/polyglot-iden/*/vectors-en.txt polyglot.iden.txt


