import imp
signif = imp.load_source('scripts/bootstrapSign.py', 'scripts/bootstrapSign.py')

names = ['arMoT', 'BERT']

def getLangs(path):
    data = []
    for line in open(path):
        if len(line) > 2:
            data.append(line.split('\t')[0])
        else: 
            data.append('')
    return data

def getScore(gold, pred, clas):
    scores = []
    cor = 0
    total = 0
    sentScores = []
    sentCor = 0
    sentTotal = 0
    for goldWord, predWord in zip(gold, pred):
        if goldWord == '' and sentTotal != 0:
            sentScores.append(sentCor/sentTotal)
            sentCor = 0
            sentTotal = 0
            continue
        if goldWord == clas or clas == '':
            total += 1
            sentTotal += 1
            if goldWord == predWord:
                cor += 1
                sentCor += 1
    return sentScores, cor/total

scores = {}
fullSentScores = {}
for lang in ['trde', 'iden']:
    goldData = getLangs('data/' + lang + '/langFeat/train')
    scores[lang] = {}
    fullSentScores[lang] = {}
    for setting in ['predicted-marmot', 'predicted-bilty', 'predicted-bert']:
        predData = getLangs('data/' + lang + '/' + setting + '/train')
        scores[lang][setting] = []
        fullSentScores[lang][setting] = []
        for langClass in [lang[:2], lang[-2:], 'un', '']:
            sentScores, score = getScore(goldData, predData, langClass)
            scores[lang][setting].append(score)
            fullSentScores[lang][setting].append(sentScores)
        
rows = ['lang1 (tr/id)', 'lang2 (de/en)', 'un', 'total']
for idx in range(len(rows)):
    print(rows[idx], end= '')
    for lang in ['iden', 'trde']:
        print(' & {:.2f}'.format(scores[lang]['predicted-marmot'][idx] * 100), end='')

        data1 = fullSentScores[lang]['predicted-marmot'][idx]
        data2 = fullSentScores[lang]['predicted-bilty'][idx]
        if signif.getP([data1, data2]) < .05:
            print(' & $^*${:.2f}'.format(scores[lang]['predicted-bilty'][idx] * 100), end='')
        else:
            print(' & {:.2f}'.format(scores[lang]['predicted-bilty'][idx] * 100), end='')

        data1 = fullSentScores[lang]['predicted-bilty'][idx]
        data2 = fullSentScores[lang]['predicted-bert'][idx]
        if signif.getP([data1, data2]) < .05:
            print(' & $^*${:.2f}'.format(scores[lang]['predicted-bert'][idx] * 100), end='')
        else:
            print(' & {:.2f}'.format(scores[lang]['predicted-bert'][idx] * 100), end='')
    print('\\\\')
        
    

