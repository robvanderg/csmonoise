import sys

if len(sys.argv) < 5:
    print('please give split ratio, norm file and train/test path. For example:')
    print('python3 split.py all.norm 0.8 train.norm test.norm')
    print()
    print('Can also give two split ratios, to generate dev and test at the same time. For example:')
    print('python3 split.py all.norm 0.6 0.8 train.norm dev.norm test.norm')
    exit(1)

def readNorm(inPath):
    data = []
    curSent = []
    for line in open(inPath):
        if len(line.strip()) < 1:
            data.append(curSent)
            curSent = []
        else:
            curSent.append(line[:-1])

    #in case file does not end with empty line
    if len(curSent) > 0:
        data.append(curSent)
    return data

def write(data, path):
    outFile = open(path, 'w')
    for sent in data:
        outFile.write('\n'.join(sent) + '\n\n')
    outFile.close()

data = readNorm(sys.argv[1])

if len(sys.argv) == 5:
    ratio = float(sys.argv[2])
    split = int(len(data) * ratio)
    print(split, len(data))

    write(data[:split], sys.argv[3])
    write(data[split:], sys.argv[4])

if len(sys.argv) == 7:
    ratio1 = float(sys.argv[2])
    split1 = int(len(data) * ratio1)
    ratio2 = float(sys.argv[3])
    split2 = int(len(data) * ratio2)
    print(split1, split2, len(data))

    write(data[:split1], sys.argv[4])
    write(data[split1:split2], sys.argv[5])
    write(data[split2:], sys.argv[6])


