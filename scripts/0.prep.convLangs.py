import sys

transf = {'AMBIG': 'un', 'DE': 'de', 'LANG3': 'un', 'MIXED': 'de', 'NE.AMBIG': 'un', 'NE.DE': 'de', 'NE.LANG3': 'un', 'NE.MIXED': 'un', 'NE.OTHER': 'un', 'NE.TR': 'tr', 'OTHER': 'un', 'TR':'tr'}

newData = []
for line in open(sys.argv[1]):
    if len(line) < 2:
        newData.append('\n')
        continue
    langLabel = line[:line.find('\t')]
    newLine = transf[langLabel] + line[line.find('\t'):]
    newData.append(newLine)

outFile = open(sys.argv[1], 'w')
for line in newData:
    outFile.write(line)
outFile.close()

