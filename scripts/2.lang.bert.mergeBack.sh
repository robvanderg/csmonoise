mkdir -p data/trde/predicted-bert
mkdir -p data/iden/predicted-bert

cat mtp/logs/iden.*/*/iden.dev.out > data/iden/predicted-bert/train
cat mtp/logs/trde.*/*/trde.dev.out > data/trde/predicted-bert/train

python3 scripts/kfold.py 10 data/trde/predicted-bert/train data/trde/predicted-bert/10fold
python3 scripts/kfold.py 10 data/iden/predicted-bert/train data/iden/predicted-bert/10fold

