import sys

for line in open(sys.argv[1]):
    tok = line.strip().split('\t')
    for i in range(len(tok)):
        tok[i] = tok[i].strip()
    if len(tok) == 1:
        print()
    else:
        print('\t'.join([tok[3], tok[1], 'PLACEHOLDER', tok[2]]))
