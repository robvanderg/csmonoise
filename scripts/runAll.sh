./scripts/0.prep.sh
# Download links of embeddings are offline, but the merged embeddings
#  can be found in the root of the repo instead.
#./scripts/0.prep.embeds.sh
./scripts/0.prep.bilty.sh
./scripts/0.prep.bert.sh

python3 scripts/1.runBaseline.py

python3 scripts/1.old.run.py > 1.old.run.sh
chmod +x 1.old.run.sh
./1.old.run.sh

./scripts/2.lang.marmot.sh

python3 scripts/2.lang.bilty.run.py > 2.lang.bilty.run.sh
chmod +x 2.lang.bilty.run.sh
./2.lang.bilty.run.sh
./scripts/2.lang.bilty.mergeBack.sh

python3 scripts/2.lang.bert.run.py > 2.lang.bert.run.sh
chmod +x 2.lang.bert.run.sh
./2.lang.bert.run.sh
./scripts/2.lang.bert.mergeBack.sh

./scripts/3.frags.split.sh

python3 scripts/4.norm.run.py > 4.run.sh
chmod +x 4.run.sh
./4.run.sh

./scripts/5.pos.prep.sh

python3 scripts/5.pos.bilty.py > 5.pos.bilty.sh
chmod +x 5.pos.bilty.sh
./5.pos.bilty.sh

python3 scripts/5.pos.bert.py > 5.pos.bert.sh
chmod +x 5.pos.bert.sh
./5.pos.bert.sh

./scripts/5.pos.collect.sh

python3 scripts/6.test.lang.py > 6.test.lang.sh 
chmod +x 6.test.lang.sh
./test.lang.sh

python3 scripts/6.test.norm.py > 6.test.norm.sh 
chmod +x 6.test.norm.sh
./test.norm.sh

./scripts/6.test.pos.prep.sh

python3 scripts/6.test.pos.py > 6.test.pos.py
chmod +x 6.test.pos.py
./test.pos.py

./scripts/7.repro.prep.sh
python3 scripts/7.repro.run.py > 7.repro.sh
chmod +x 7.repro.sh
./7.repro.sh

