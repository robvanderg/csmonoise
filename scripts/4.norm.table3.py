import os
import utils
import imp
signif = imp.load_source('scripts/bootstrapSign.py', 'scripts/bootstrapSign.py')

def ratio(path):
    raw, gold, _ = utils.loadNormData(path)
    total = 0
    normed = 0
    for rawSent, goldSent in zip(raw, gold):
        for rawWord, goldWord in zip(rawSent, goldSent):
            if rawWord != goldWord:
                normed += 1
            total += 1
    return 1 - (normed/total)

def ratioPerLang(path):
    raw, gold, lang = utils.loadNormData(path)
    total1 = 0
    normed1 = 0
    total2 = 0
    normed2 = 0
    for langSent, rawSent, goldSent in zip(lang, raw, gold):
        for langWord, rawWord, goldWord in zip(langSent, rawSent, goldSent):
            if langWord in ['tr', 'id']:
                if rawWord != goldWord:
                    normed1 += 1
                total1 += 1
            elif langWord in ['de', 'en']:
                if rawWord != goldWord:
                    normed2 += 1
                total2 += 1
    return 1 - (normed1/total1), 1 - (normed2/total2)

def getScorePerLang(goldData, predData, verbose=False):
    tp = 0
    fp = 0 
    fn = 0
    tn = 0
    sentScores = []
    for goldLine, predLine in zip(goldData, predData):
        if len(goldLine.split('\t')) > 2:
            pred = predLine.strip()
            gold = goldLine.strip().split('\t')[-1].strip()
            orig = goldLine.strip().split('\t')[1].strip()
            needNorm = orig != gold
            cor = pred == gold
            if needNorm:
                if cor:
                    tp += 1
                else:
                    fn += 1
            else:
                if cor:
                    tn += 1
                else:
                    fp += 1

    recall = 0.0 if tp+fn == 0 else tp / (tp + fn)
    precision = 0.0 if tp+fp == 0 else tp / (tp + fp)
    return recall, precision

def get10foldData(path):
    data = []
    for fold in range(10):
        for line in open(path + '.' + str(fold)):
            data.append(line)
    return data

    
def getRaw(data):
    newData = []
    for item in data:
        tok = item.split('\t')
        if len(tok) > 1:
            newData.append(tok[1])
        else:
            newData.append('')
    return newData

scores = {}
allSentScores = {}
for langs in [['id', 'en'], ['tr', 'de']]:
    name = ''.join(langs)
    goldData = open('data/' + name + '/langFeat/train').readlines()
    scores[name] = {}
    scores[name][langs[0]] = []
    scores[name][langs[1]] = []


    lai = getRaw(goldData)
    score1, score2 = getScorePerLang(goldData, lai)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)

    predData = get10foldData('preds/mfr.' + name + '.norm')
    score1, score2 = getScorePerLang(goldData, predData)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)

    predData1 = get10foldData('preds/old.' + name + '.' + langs[0] + '.norm')
    predData2 = get10foldData('preds/old.' + name + '.' + langs[1] + '.norm')
    score1, score2 = getScorePerLang(goldData, predData1)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)
    score1, score2 = getScorePerLang(goldData, predData2)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)

    goldFragData1 = [langs[0] + '\t' + line for line in open('data/' + name + '/frags-bert/train.' + langs[0]).readlines()]
    goldFragData2 = [langs[1] + '\t' + line for line in open('data/' + name + '/frags-bert/train.' + langs[1]).readlines()]
    goldFragData = goldFragData1 + goldFragData2
    predData1 = get10foldData('preds/frags-bert.' + name + '.' + langs[0] + '.norm')
    predData2 = get10foldData('preds/frags-bert.' + name + '.' + langs[1] + '.norm')
    predData = predData1 + predData2

    score1, score2 = getScorePerLang(goldFragData, predData)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)

    predData = get10foldData('preds/multiling.' + name + '.norm')
    score1, score2 = getScorePerLang(goldData, predData)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)

    predData = get10foldData('preds/predicted-bert.' + name + '.norm')
    score1, score2 = getScorePerLang(goldData, predData)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)

print(' & \multicolumn{2}{c}{iden} & \multicolumn{2}{c}{trde} \\\\')
print('Model & recall & precision & recall & precision \\\\')
names = ['LAI', 'MFR', 'Monolingual-lang1 (tr/id)', 'Monolingual-lang2 (de/en)', 'Frags', 'Multilingual', 'Language-aware']
for modelIdx in range(len(scores['iden']['id'])):
    row = [names[modelIdx]]
    for langs in [['id', 'en'], ['tr', 'de']]:
        name = ''.join(langs)
        for lang in langs:
            row.append('{:.2f}'.format(scores[name][lang][modelIdx]*100))
    print(' & '.join(row) + ' \\\\')
print()

         


