## extend danish embeddings with german ones (take homographs from danish only)
import codecs
import sys

if len(sys.argv) < 4:
    print('please provide: <emb1> <emb2> <out>')
    exit(1)

data = {}

#for i, line in enumerate(open("dumped/debug/y1gzn8mg2l/vectors-da.txt")):
for i, line in enumerate(open(sys.argv[1])):
    if i>0:
        tok = line.strip().split()
        word = tok[0]
        if len(tok) == 65:
            data[word] = [float(x) for x in tok[1:]]
print("loaded", len(data))

for i, line in enumerate(open(sys.argv[2])):
    if i>0:
        tok = line.strip().split()
        word = tok[0]
        if len(tok) == 65:
            if word in data:
                for i in range(len(tok)-1):
                    data[word][i] = (data[word][i] + float(tok[i+1])) / 2
            else:
                data[word] = [float(x) for x in tok[1:]]

print("total size", len(data))

OUT=codecs.open(sys.argv[3],"w", encoding="utf-8")
for word in data:
    OUT.write(' '.join([word] + [str(val) for val in data[word]]) + '\n')
OUT.close()
