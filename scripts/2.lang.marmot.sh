mkdir -p data/trde/predicted-marmot
mkdir -p data/iden/predicted-marmot

cut -f 6 data/TrDeNormalisation/lidout/iden.dev.marmot_lid.conll09 > data/iden/predicted-marmot/train.lang
cut -f 6 data/TrDeNormalisation/lidout/trde.dev.marmot_lid.conll09 > data/trde/predicted-marmot/train.lang

paste data/trde/predicted-marmot/train.lang data/trde/norm/train > data/trde/predicted-marmot/train
paste data/iden/predicted-marmot/train.lang data/iden/norm/train > data/iden/predicted-marmot/train

python3 scripts/kfold.py 10 data/trde/predicted-marmot/train data/trde/predicted-marmot/10fold
python3 scripts/kfold.py 10 data/iden/predicted-marmot/train data/iden/predicted-marmot/10fold

