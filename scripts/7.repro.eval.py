import os

def readData(path):
    data = []
    curSent = []
    for line in open(path):
        line = line.lower()
        tok = line[:-1].split('\t')
        if tok == [''] or tok == []:
            data.append(curSent)
            curSent = []
        else:
            curSent.append(tok)
    if curSent != []:
        data.append(curSent)
    return data

def readData2(path, path2):
    data = []
    curSent = []
    for line, line2 in zip(open(path), open(path2)):
        line = line.lower()
        line2 = line2.lower()
        tok = line[:-1].split('\t')
        if tok == [''] or tok == []:
            data.append(curSent)
            curSent = []
        else:
            tok[2] = line2[:-1]
            curSent.append(tok)

    if curSent != []:
        data.append(curSent)
    return data

def getRepls(data):
    repl = {}
    for sent in data:
        for word in sent:
            if word[0] not in repl:
                repl[word[0]] = []
            if word[0] != word[2]:
                #if word[0] not in repl:
                #    repl[word[0]] = []
                repl[word[0]].append(word[2])
    return repl

os.system('cat preds/repro.iden.norm.* > preds/repro.iden.norm.all')
goldData = readData('data/iden/repro/all')
predData = readData2('data/iden/repro/all', 'preds/repro.iden.norm.all')

print(len(predData))
print(len(goldData))
replGold = getRepls(goldData)
replPred = getRepls(predData)

optimist = 0
pessimist = 0
totalNormed = 0
avgTotal = 0
avgFound = 0
for word in replGold:
    found = 0
    total = len(replGold[word])
    changed = False
    for norm in replGold[word]:
        if norm in replPred[word]:
            found += 1
        if norm != word:
            changed = True
    if changed:
        print(total)
        if found > 0:
            optimist += 1
        if found == total:
            pessimist += 1
        avgTotal += total
        avgFound += found
        totalNormed += 1
print('optimist:  ', optimist/totalNormed)
print('pessimist: ', pessimist/totalNormed)
print('avg:       ', avgFound/avgTotal)
    


