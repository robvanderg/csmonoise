mkdir -p data/trde/pos/

wget https://github.com/UniversalDependencies/UD_German-GSD/archive/r2.5.tar.gz
tar -zxvf r2.5.tar.gz
rm r2.5.tar.gz

wget https://github.com/UniversalDependencies/UD_Turkish-IMST/archive/r2.5.tar.gz
tar -zxvf r2.5.tar.gz
rm r2.5.tar.gz

python3 scripts/5.pos.mergeUDs.py UD_Turkish-IMST-r2.5/tr_imst-ud-train.conllu UD_German-GSD-r2.5/de_gsd-ud-train.conllu data/trde/pos/ud-tr-de.train
python3 scripts/5.pos.mergeUDs.py UD_Turkish-IMST-r2.5/tr_imst-ud-dev.conllu UD_German-GSD-r2.5/de_gsd-ud-dev.conllu data/trde/pos/ud-tr-de.dev


cut -f 4 data/TrDeNormalisation/data/whole/trde_twitter.whole.simp_orig_norm.giza_man.algn.pos > posTmp
python3 scripts/split.py posTmp 0.8 posTmp.train posTmp.test
mv posTmp.train posTmp
rm posTmp.test
cut -f 2 data/trde/langFeat/train > raw
cut -f 4 data/trde/langFeat/train > gold
cat preds/multiling.trde.norm.* > norm #preds/test.multiling.trde.norm > norm
cat preds/predicted-bert.trde.norm.* > langaware #preds/test.langaware.trde.norm > langaware
mkdir -p data/trde/pos
paste raw posTmp > data/trde/pos/raw.dev
paste gold posTmp > data/trde/pos/gold.dev
paste norm posTmp > data/trde/pos/multiling.dev
paste langaware posTmp > data/trde/pos/langaware.dev

rm raw gold norm langaware posTmp

sed -i "s;^	$;;g" data/trde/pos/*

python3 scripts/5.pos.expand.py

