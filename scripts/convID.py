import csv
import sys

with open(sys.argv[1]) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    first = True
    for row in csv_reader:
        if first:
            first = False
            continue
        cmd = 'raws = ' + row[3].replace('list(', ',').replace('])]', ']]')
        exec(cmd)
        raws = raws[1]
        cmd = 'norms = ' + row[5]
        exec(cmd)
        cmd = 'langs = ' + row[4]
        exec(cmd)
        for raw, norm, lang in zip(raws, norms, langs):
            if ' ' in raw:
                if len(raw.split(' ')) == len(norm.split(' ')):
                    for rawSplit, normSplit in zip(raw.split(' '), norm.split(' ')):
                        print('\t'.join([lang, rawSplit, 'PLACEHOLDER', normSplit]))
                elif '-' in norm:
                    print('\t'.join([lang, raw.split(' ')[0], 'PLACEHOLDER', norm]))
                    print('\t'.join([lang, raw.split(' ')[1], 'PLACEHOLDER', '']))
                elif norm[:4] == 'the ':
                    for rawSplit, normSplit in zip(raw.split(' '), norm.split(' ')[1:]):
                        print('\t'.join([lang, rawSplit, 'PLACEHOLDER', normSplit]))
                else:
                    print('\t'.join([lang, raw.split(' ')[0], 'PLACEHOLDER', norm]))
                    print('\t'.join([lang, raw.split(' ')[1], 'PLACEHOLDER', '']))
            else:
                print('\t'.join([lang, raw, 'PLACEHOLDER', norm]))
        print() 
