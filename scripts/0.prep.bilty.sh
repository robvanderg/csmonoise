mkdir -p bilty
cut -f 1 data/trde/langFeat/train > bilty/trde.langs
cut -f 2 data/trde/langFeat/train > bilty/trde.words
paste bilty/trde.words bilty/trde.langs > bilty/trde.train

cut -f 1 data/iden/langFeat/train > bilty/iden.langs
cut -f 2 data/iden/langFeat/train > bilty/iden.words
paste bilty/iden.words bilty/iden.langs > bilty/iden.train

python3 scripts/kfold.py 10 bilty/trde.train bilty/trde.kfold
python3 scripts/kfold.py 10 bilty/iden.train bilty/iden.kfold

git clone https://github.com/bplank/bilstm-aux.git
cd bilstm-aux
git reset --hard 2360bed
cd ..

