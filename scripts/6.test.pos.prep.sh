
cut -f 4 data/TrDeNormalisation/data/whole/trde_twitter.whole.simp_orig_norm.giza_man.algn.pos > posTmp
python3 scripts/split.py posTmp 0.8 posTmp.train posTmp.test
rm posTmp.train
mv posTmp.test posTmp
cut -f 2 data/trde/langFeat/test > raw
cut -f 4 data/trde/langFeat/test > gold
#mkdir -p data/trde/pos
paste raw posTmp > data/trde/pos/raw.test
paste gold posTmp > data/trde/pos/gold.test
paste preds/test.multiling.trde.norm posTmp > data/trde/pos/multiling.test
paste preds/test.langaware.trde.norm posTmp > data/trde/pos/langaware.test
paste preds/test.old.trde.tr.norm posTmp > data/trde/pos/old.tr.test
paste preds/test.old.trde.de.norm posTmp > data/trde/pos/old.de.test

rm raw gold posTmp mfr.tmp

sed -i "s;^	$;;g" data/trde/pos/*

python3 scripts/5.pos.expand.py

