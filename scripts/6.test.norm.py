from pprint import pprint
import os

if not os.path.isdir('config'):
    os.mkdir('config')

datasets = ['trde', 'iden']
for dataset in datasets:
    cmd = 'python3 scripts/1.baseline.py --method LAI --train data/' + dataset + '/norm/train --dev data/' + dataset + '/norm/test --out preds/test.lai.' + dataset + '.norm'
    print(cmd)

    for lang in [dataset[:2], dataset[2:]]:
        dataDir = '../../data/' + lang 
        trainFile = '../../data/' + dataset + '/norm/train'
        testFile = '../../data/' + dataset + '/norm/test'
        modelPath = '../../working/old.' + dataset + '.' + lang 
        normFile = '../../preds/test.old.' + dataset + '.' + lang + '.norm'
        outFile = '../../preds/test.old.' + dataset + '.' + lang + '.out'
        cmd = 'cd monoise/src &&'
        cmd += ' ./tmp/bin/binary -m TR -i ' + trainFile + ' -r ' + modelPath
        cmd += ' -d ' + dataDir + ' -C -f 111101111101'
        cmd += ' &> ' + outFile + ' && '
        cmd += ' ./tmp/bin/binary -m RU -W -i ' + testFile + ' -d ' + dataDir
        cmd += ' -r ' + modelPath + ' -C -f 111101111101 > ' + normFile
        cmd += ' && cd ../../'
        print(cmd)

    
    cmd = 'cd src && ./tmp/bin/binary -m TR -r ../working/norm.' + dataset + ' -i ../data/' + dataset + '/norm/train -d ../data/tr -2 ../data/de -C -f 111101111101 && ' 
    cmd += './tmp/bin/binary -m RU -W -r ../working/norm.' + dataset + ' -i ../data/' + dataset + '/norm/test -d ../data/tr -2 ../data/de -C -f 111101111101 > ../preds/test.multiling.' + dataset + '.norm && cd ../'
    print(cmd)

    cmd = 'cd src && ./tmp/bin/binary -m TR -r ../working/predicted-bert.' + dataset + ' -E -i ../data/' + dataset + '/predicted-bert/train -d ../data/tr -2 ../data/de -C -f 111101111101 && ' 
    cmd += './tmp/bin/binary -m RU -W -r ../working/predicted-bert.' + dataset + ' -E -i ../data/' + dataset + '/predicted-bert/test -d ../data/tr -2 ../data/de -C -f 111101111101 > ../preds/test.langaware.' + dataset + '.norm && cd ../'
    print(cmd)
    
