import os
import sys
import utils


def write(outFile, lang, rawData, goldData, langsData):
    for sentIdx in range(len(rawData)):
        prevWritten = False
        for wordIdx in range(0, len(rawData[sentIdx])):
            if langsData[sentIdx][wordIdx] == lang:
                outFile.write(rawData[sentIdx][wordIdx] + '\tPLACEHOLDER\t' + goldData[sentIdx][wordIdx] + '\n')
                prevWritten = True
            else:
                if prevWritten:
                    outFile.write('\n')
                prevWritten = False
        if langsData[sentIdx][-1] == lang:
            outFile.write('\n')
    

for langPair in [['id', 'en'], ['tr', 'de']]:
    path = 'data/' + ''.join(langPair) + '/' 
    if not os.path.isdir(path + sys.argv[1]):
        os.mkdir(path + sys.argv[1])
    if sys.argv[1] == 'frags-gold':
        rawData, goldData, langsData = utils.loadNormData(path + 'langFeat/train')
    if sys.argv[1] == 'frags-marmot':
        rawData, goldData, langsData = utils.loadNormData(path + 'predicted-marmot/train')
    if sys.argv[1] == 'frags-bilty':
        rawData, goldData, langsData = utils.loadNormData(path + 'predicted-bilty/train')
    if sys.argv[1] == 'frags-bert':
        rawData, goldData, langsData = utils.loadNormData(path + 'predicted-bert/train')
    outfile1 = open(path + sys.argv[1] + '/train.' + langPair[0], 'w')
    outfile2 = open(path + sys.argv[1] + '/train.' + langPair[1], 'w')
    splits = []
    # transform un to prev word:
    for sentIdx in range(len(langsData)):

        if langsData[sentIdx][0] == 'un':
            nextLabel = ''
            for wordIdx in range(1, len(langsData[sentIdx])):
                if langsData[sentIdx][wordIdx] != 'un':
                    nextLabel = langsData[sentIdx][wordIdx]
                    break
            langsData[sentIdx][0] = nextLabel

        for wordIdx in range(1, len(langsData[sentIdx])):
            if langsData[sentIdx][wordIdx] == 'un':
                langsData[sentIdx][wordIdx] = langsData[sentIdx][wordIdx-1]

    write(outfile1, langPair[0], rawData, goldData, langsData)
    write(outfile2, langPair[1], rawData, goldData, langsData)
    outfile1.close()
    outfile2.close()


