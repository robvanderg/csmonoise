

for setting in ['raw', 'multiling', 'langaware', 'gold']:
    modelPath = 'pos.' + setting
    trainPath = '../data/trde/pos/ud-tr-de.train'
    devPath = '../data/trde/pos/ud-tr-de.dev'
    testPath = '../data/trde/pos/' + setting + '.dev.txt'
    cmd = 'cd bilstm-aux && python3 src/structbilty.py --model ' + modelPath + ' --train ' + trainPath
    cmd += ' --dev ' + devPath + ' --test ' + testPath + ' --output ../posPreds/bilty.' + setting + '.out' 
    cmd += ' --embeds ../polyglot.trde.txt && cd ../'
    print(cmd)

