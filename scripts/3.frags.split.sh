mkdir -p data/trde/frags-gold
mkdir -p data/iden/frags-gold
mkdir -p data/trde/frags-marmot
mkdir -p data/iden/frags-marmot
mkdir -p data/trde/frags-bilty
mkdir -p data/iden/frags-bilty
mkdir -p data/trde/frags-bert
mkdir -p data/iden/frags-bert

python3 scripts/3.frags.prep.py frags-gold
python3 scripts/3.frags.prep.py frags-marmot
python3 scripts/3.frags.prep.py frags-bilty
python3 scripts/3.frags.prep.py frags-bert

python3 scripts/kfold.py 10 data/trde/frags-gold/train.tr data/trde/frags-gold/tr.10fold
python3 scripts/kfold.py 10 data/trde/frags-gold/train.de data/trde/frags-gold/de.10fold
python3 scripts/kfold.py 10 data/iden/frags-gold/train.id data/iden/frags-gold/id.10fold
python3 scripts/kfold.py 10 data/iden/frags-gold/train.en data/iden/frags-gold/en.10fold

python3 scripts/kfold.py 10 data/trde/frags-marmot/train.tr data/trde/frags-marmot/tr.10fold
python3 scripts/kfold.py 10 data/trde/frags-marmot/train.de data/trde/frags-marmot/de.10fold
python3 scripts/kfold.py 10 data/iden/frags-marmot/train.id data/iden/frags-marmot/id.10fold
python3 scripts/kfold.py 10 data/iden/frags-marmot/train.en data/iden/frags-marmot/en.10fold

python3 scripts/kfold.py 10 data/trde/frags-bilty/train.tr data/trde/frags-bilty/tr.10fold
python3 scripts/kfold.py 10 data/trde/frags-bilty/train.de data/trde/frags-bilty/de.10fold
python3 scripts/kfold.py 10 data/iden/frags-bilty/train.id data/iden/frags-bilty/id.10fold
python3 scripts/kfold.py 10 data/iden/frags-bilty/train.en data/iden/frags-bilty/en.10fold

python3 scripts/kfold.py 10 data/trde/frags-bert/train.tr data/trde/frags-bert/tr.10fold
python3 scripts/kfold.py 10 data/trde/frags-bert/train.de data/trde/frags-bert/de.10fold
python3 scripts/kfold.py 10 data/iden/frags-bert/train.id data/iden/frags-bert/id.10fold
python3 scripts/kfold.py 10 data/iden/frags-bert/train.en data/iden/frags-bert/en.10fold

