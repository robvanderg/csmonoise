for langPair in [['id', 'en'], ['tr', 'de']]:
    name = ''.join(langPair)
    for lang in langPair:
        dataDir = '../../data/' + lang
        for fold in range(10):
            trainFile = '../../data/' + name + '/norm/10fold.train.' + str(fold)
            devFile = '../../data/' + name + '/norm/10fold.dev.' + str(fold)
            modelPath = '../../working/old.' + name + '.' + lang + '.' + str(fold)
            normFile = '../../preds/old.' + name + '.' + lang + '.norm.' + str(fold)
            outFile = '../../preds/old.' + name + '.' + lang + '.out.' + str(fold)
            cmd = 'cd monoise/src &&'
            cmd += ' ./tmp/bin/binary -m TR -i ' + trainFile + ' -r ' + modelPath 
            cmd += ' -d ' + dataDir + ' -C -f 111101111101'
            cmd += ' &> ' + outFile + ' && ' 
            cmd += ' ./tmp/bin/binary -m RU -W -i ' + devFile + ' -d ' + dataDir 
            cmd += ' -r ' + modelPath + ' -C -f 111101111101 > ' + normFile
            cmd += ' && cd ../../'
            print(cmd)

