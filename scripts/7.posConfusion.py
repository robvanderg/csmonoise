import os
import matplotlib.pyplot as plt
import matplotlib as mpl

norms = ['raw', 'multiling', 'langaware', 'gold']
poss = ['marmot', 'bilty', 'bert']

def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

def readPos(path):
    data = []
    curSent = []
    for line in open(path):
        tok = line[:-1].split('\t')
        if len(tok) < 2:
            data.append(curSent)
            curSent = []
        else:
            if tok[-1] == 'CONJ':
                tok[-1] = 'CCONJ'
            curSent.append(tok)
    return data

def eval(goldPath, predPath, settingName = ''):
    confusions = {}
    golds = readPos(goldPath)
    preds = readPos(predPath)
    for goldSent, predSent in zip(golds, preds):
        predTags = []
        predIdx = 0
        for goldWordIdx, goldWord in enumerate(goldSent):
            if ' ' in goldWord[0]:
                count = goldWord[0].count(' ')
                tags = []
                for i in range(count +1):
                    tags.append(predSent[predIdx+i][-1])
                predTags.append(',,'.join(tags))
                predIdx += count + 1
            elif goldWord[0] == '':
                if predIdx < len(predSent):
                    predTags.append("NEXT-" + predSent[predIdx][-1])
                else:
                    predTags.append("NEXT-" + predSent[-1][-1])
            else:
                predTags.append(predSent[predIdx][-1])
                predIdx += 1

        mergeOptions = []
        idx = -1
        for predTag, goldTag in zip(predTags, goldSent):
            idx +=1 
            goldWord = goldTag[0]
            goldTag = goldTag[-1]
            predTagOptions = predTag.split(',,')
            mergeOptions.append(goldTag)
            if predTag.startswith('NEXT'):
                continue
            else:
                if len(intersection(mergeOptions, predTagOptions)) == 0:
                    for goldTag in mergeOptions:
                        for predTag in predTagOptions:
                            name = goldTag + '-' + predTag
                            if name not in confusions:
                                confusions[name] = 0
                            confusions[name] += 1
                            if settingName != '':
                                print('CONF.' + settingName, name, goldSent[idx][0])
                mergeOptions = []
    return confusions

if __name__ == '__main__':
    for pos in poss:
        prevScores = []
        row = [pos]
        table = []
        for norm in norms:
            pred = 'posPreds/' + pos + '.' + norm + '.out'
            if pos == 'bilty':
                pred += '.task0'
            gold = 'data/trde/pos/' + norm + '.dev'
            confusions = eval(gold, pred)
            #confusions = eval(gold, pred, pos + '.' + norm)
            allTags = sorted(set([x.split('-')[1] for x in confusions] + [x.split('-')[0] for x in confusions]))

            # graph
            scores = []
            for _ in range(len(allTags)):
                scores.append([0] * len(allTags))
            for conf in confusions:
                goldIdx = allTags.index(conf.split('-')[0])
                predIdx = allTags.index(conf.split('-')[1])
                scores[goldIdx][predIdx] = confusions[conf]
            fig, ax = plt.subplots(figsize=(8,5), dpi=300)
            cax = ax.imshow(scores, vmax=500)
            ax.set_ylabel('Gold')
            ax.set_xlabel('Pred')
            ax.set_xticks(range(len(allTags)))
            ax.set_yticks(range(len(allTags)))

            ax.set_yticklabels(allTags)
            ax.set_xticklabels(allTags)
            titleName = pos + '-' + norm
            if pos == 'bilty':
                titleName = 'Bilty-' + norm
            elif pos == 'bert':
                titleName = 'MaChAmp-' + norm
            ax.set_title(titleName)
            plt.setp(ax.get_xticklabels(), rotation=45, ha="right", va='top',
                     rotation_mode="anchor")
            fig.colorbar(cax)
            plt.savefig(pos + '.' + norm + '.pdf', bbox_inches='tight')
        
            #Table
            if table == []:
                table.append([])
                for confusion in sorted(confusions, key=confusions.get, reverse=True)[:10]:
                    table[-1].append(confusion)
            table.append([])
            for confusion in table[0]:
                table[-1].append(str(confusions[confusion]))
            total = 0
            for confusion in confusions:
                total += confusions[confusion]
            print(pos, norm, total)
            #print(pos, norm, '(GOLD-PRED)')
            #for confusion in sorted(confusions, key=confusions.get, reverse=True)[:10]:
            #    print(confusion + ' & ' + str(confusions[confusion]) + '\\\\')
            #print()
        print(pos)
        print(' & LAI & Multilingual & Lang-aware & Gold \\\\')
        for x in range(len(table[0])):
            data = [table[y][x] for y in range(len(table))]
            for i in range(2, len(data)):
                data[i] = str(int(data[i]) - int(data[1]))
                if data[i][0] != '-':
                    data[i] = '+' + data[i]
            print(' & '.join(data) + ' \\\\')
        print()

