import sys

if len(sys.argv) < 4:
    print('please give number of folds, norm file and output path For example:')
    print('python3 split.py 10 train.norm 10fold')
    print()
    exit(1)

def readNorm(inPath):
    data = []
    curSent = []
    for line in open(inPath):
        tok = line.strip().split('\t')
        if len(tok) == 0 or tok == ['']:
            data.append(curSent)
            curSent = []
        else:
            curSent.append(line[:-1])

    #in case file does not end with empty line
    if len(curSent) > 0:
        data.append(curSent)
    return data

def write(data, path):
    outFile = open(path, 'w')
    for sent in data:
        outFile.write('\n'.join(sent) + '\n\n')
    outFile.close()

splits = int(sys.argv[1])
data = readNorm(sys.argv[2])

splitsSize = len(data)/splits

for i in range(splits):
    devBeg = int(i * splitsSize)
    devEnd = int((i+1) * splitsSize)
    if i == splits-1:
        devEnd = len(data)
    write(data[devBeg:devEnd], sys.argv[3] + '.dev.' + str(i))
    write(data[0:devBeg] + data[devEnd:], sys.argv[3] + '.train.' + str(i))
    print(devBeg, devEnd, devEnd-devBeg)



