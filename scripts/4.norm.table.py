import os
import utils
import imp
signif = imp.load_source('scripts/bootstrapSign.py', 'scripts/bootstrapSign.py')

def ratio(path):
    raw, gold, _ = utils.loadNormData(path)
    total = 0
    normed = 0
    for rawSent, goldSent in zip(raw, gold):
        for rawWord, goldWord in zip(rawSent, goldSent):
            if rawWord != goldWord:
                normed += 1
            total += 1
    return 1 - (normed/total)

def ratioPerLang(path):
    raw, gold, lang = utils.loadNormData(path)
    total1 = 0
    normed1 = 0
    total2 = 0
    normed2 = 0
    for langSent, rawSent, goldSent in zip(lang, raw, gold):
        for langWord, rawWord, goldWord in zip(langSent, rawSent, goldSent):
            if langWord in ['tr', 'id']:
                if rawWord != goldWord:
                    normed1 += 1
                total1 += 1
            elif langWord in ['de', 'en']:
                if rawWord != goldWord:
                    normed2 += 1
                total2 += 1
    return 1 - (normed1/total1), 1 - (normed2/total2)

def getScore(goldData, predData, verbose=False):
    total = 0
    correct = 0
    sentCor = 0
    sentTotal = 0
    sentScores = []
    for goldLine, predLine in zip(goldData, predData):
        if len(goldLine.strip()) > 1:
            if predLine.strip() == goldLine.strip().split('\t')[-1].strip():
                correct += 1
                sentCor += 1
            elif verbose:
                print(goldLine.strip().split('\t')[-1].strip(), predLine.strip())
            total += 1
            sentTotal += 1
        else:
            sentScores.append(sentCor/sentTotal)
            sentCor = 0
            sentTotal = 0
    return sentScores, correct/total

def get10foldData(path):
    data = []
    for fold in range(10):
        for line in open(path + '.' + str(fold)):
            data.append(line)
    return data

# Not used anymore; but useful for analysis
def getScorePerLang(goldData, predData):
    if not os.path.isfile(pred):
        print('ERROR', pred, 'not found')
        return 0.0, 0.0
    totalLang1 = 0
    correctLang1 = 0
    totalLang2 = 0
    correctLang2 = 0
    for goldLine, predLine in zip(goldData, predData):
        if len(goldLine.strip()) > 1:
            goldNorm = goldLine.strip().split('\t')[-1].strip()
            lang = goldLine.strip().split('\t')[0].strip()
            if lang in ['tr', 'id']:
                if predLine.strip() == goldLine.strip().split('\t')[-1].strip():
                    correctLang1 += 1
                totalLang1 += 1
            elif lang in ['de', 'en']:
                if predLine.strip() == goldLine.strip().split('\t')[-1].strip():
                    correctLang2 += 1
                totalLang2 += 1
    return correctLang1/totalLang1, correctLang2/totalLang2
    
def getRaw(data):
    newData = []
    for item in data:
        tok = item.split('\t')
        if len(tok) > 1:
            newData.append(tok[1])
        else:
            newData.append('')
    return newData

scores = {}
allSentScores = {}
for langs in [['id', 'en'], ['tr', 'de']]:
    name = ''.join(langs)
    goldData = open('data/' + name + '/langFeat/train').readlines()
    scores[name] = []
    allSentScores[name] = []

    lai = getRaw(goldData)
    sentScores, score = getScore(goldData, lai)
    scores[name].append(score)
    allSentScores[name].append(sentScores)
    
    predData = get10foldData('preds/mfr.' + name + '.norm')
    sentScores, score = getScore(goldData, predData)
    scores[name].append(score) 
    allSentScores[name].append(sentScores)
    
    predData1 = get10foldData('preds/old.' + name + '.' + langs[0] + '.norm')
    predData2 = get10foldData('preds/old.' + name + '.' + langs[1] + '.norm')
    sentScores, score = getScore(goldData, predData1)
    scores[name].append(score) 
    allSentScores[name].append(sentScores)
    sentScores, score = getScore(goldData, predData2)
    scores[name].append(score) 
    allSentScores[name].append(sentScores)

    goldFragData1 = open('data/' + name + '/frags-bert/train.' + langs[0]).readlines()
    goldFragData2 = open('data/' + name + '/frags-bert/train.' + langs[1]).readlines()
    goldFragData = goldFragData1 + goldFragData2
    predData1 = get10foldData('preds/frags-bert.' + name + '.' + langs[0] + '.norm')
    predData2 = get10foldData('preds/frags-bert.' + name + '.' + langs[1] + '.norm')
    predData = predData1 + predData2
    sentScores, score = getScore(goldFragData, predData)
    scores[name].append(score) 
    allSentScores[name].append(sentScores)

    predData = get10foldData('preds/multiling.' + name + '.norm')
    sentScores, score = getScore(goldData, predData, False)
    scores[name].append(score) 
    allSentScores[name].append(sentScores)
    
    
    predData = get10foldData('preds/predicted-bert.' + name + '.norm')
    sentScores, score = getScore(goldData, predData)
    scores[name].append(score) 
    allSentScores[name].append(sentScores)
    

print('Model & id-en & tr-de \\\\')
names = ['LAI', 'MFR', 'Monolingual-lang1 (tr/id)', 'Monolingual-lang2 (de/en)', 'Frags', 'Multilingual', 'Language-aware']
for i in range(len(scores['iden'])):
    row = [names[i]]
    for langs in ['iden', 'trde']:
        if i > 0:
            if .05 > signif.getP([allSentScores[langs][i-1], allSentScores[langs][i]]):
                row.append('$^*${:.2f}'.format(scores[langs][i] * 100))
            else:
                row.append('{:.2f}'.format(scores[langs][i] * 100))
        else:
            row.append('{:.2f}'.format(scores[langs][i] * 100))
    print(' & '.join(row) + '\\\\')
print()

scores = {}
allSentScores = {}
for langs in [['id', 'en'], ['tr', 'de']]:
    name = ''.join(langs)
    goldData = open('data/' + name + '/langFeat/train').readlines()
    scores[name] = []
    allSentScores[name] = []
    for setting in ['marmot', 'bilty', 'bert', 'gold']:
        goldFragData1 = open('data/' + name + '/frags-' + setting + '/train.' + langs[0]).readlines()
        goldFragData2 = open('data/' + name + '/frags-' + setting + '/train.' + langs[1]).readlines()
        goldFragData = goldFragData1 + goldFragData2
        predData1 = get10foldData('preds/frags-' + setting + '.' + name + '.' + langs[0] + '.norm')
        predData2 = get10foldData('preds/frags-' + setting + '.' + name + '.' + langs[1] + '.norm')
        predData = predData1 + predData2
        sentScores, score = getScore(goldFragData, predData)
        scores[name].append(score) 
        allSentScores[name].append(sentScores)

    for setting in ['marmot', 'bilty', 'bert']:
        predData = get10foldData('preds/predicted-' + setting + '.' + name + '.norm')
        sentScores, score = getScore(goldData, predData)
        scores[name].append(score) 
        allSentScores[name].append(sentScores)
    predData = get10foldData('preds/langFeat.' + name + '.norm')
    sentScores, score = getScore(goldData, predData)
    scores[name].append(score)
    allSentScores[name].append(sentScores)

names = ['Frags (MarMoT)', 'Frags (Bilty)', 'Frags (BERT)', 'Frags (Gold)', 'Language-aware (MarMoT)', 'Language-aware (Bilty)', 'Language-aware (BERT)', 'Language-aware (Gold)']
for i in range(len(scores['iden'])):
    row = [names[i]]
    for langs in ['iden', 'trde']:
        if i > 0:
            if .05 > signif.getP([allSentScores[langs][i-1], allSentScores[langs][i]]):
                row.append('$^*${:.2f}'.format(scores[langs][i] * 100))
            else:
                row.append('{:.2f}'.format(scores[langs][i] * 100))
        else:
            row.append('{:.2f}'.format(scores[langs][i] * 100))
    print(' & '.join(row) + '\\\\')


