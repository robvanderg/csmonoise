import os

for dataset in ['trde', 'iden']:
    for fold in range(10):
        fold = str(fold)
        outFile = 'preds/mfr.' + dataset + '.norm.' + fold
        cmd = 'python3 scripts/1.baseline.py --method MFR --train data/' + dataset + '/norm/10fold.train.' + fold + ' --dev data/' + dataset + '/norm/10fold.dev.' + fold + ' --out ' + outFile
        cmd += ' && cut -f 2 ' + outFile + ' > ' + outFile + '.tmp && mv ' + outFile + '.tmp ' + outFile
        os.system(cmd)
