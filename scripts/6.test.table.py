import imp
posEval = imp.load_source('scripts/5.pos.table', 'scripts/5.pos.table.py')
signif = imp.load_source('scripts/bootstrapSign.py', 'scripts/bootstrapSign.py')

def getLangScore(goldPath, predPath):
    cor = 0
    total = 0
    for goldLine, predLine in zip(open(goldPath), open(predPath)):
        goldTok = goldLine.strip().split('\t')
        if len(goldTok) > 2:
            total += 1
            if goldTok[0] == predLine.strip().split('\t')[0]:
                cor += 1
    return cor/total

def lai(path):
    cor = 0
    total = 0
    for line in open(path):
        tok = line[:-1].split('\t')
        if len(tok) > 2:
            if tok[0] == tok[2]:
                cor += 1
            total += 1
    return cor/total

def getNormScore(goldPath, predPath, ignCaps=False):
    cor = 0
    total = 0
    scores = []
    sentScores = []
    sentCor = 0
    sentTotal = 0
    if type(goldPath) == str:
        goldLines = open(goldPath).readlines()
        predLines = open(predPath).readlines()
    else:
        goldLines = goldPath
        predLines = predPath
    for goldLine, predLine in zip(goldLines, predLines):
        if ignCaps:
            goldLine = goldLine.lower()
            predLine = predLine.lower()
        goldTok = goldLine[:-1].split('\t')
        if len(goldTok) > 2:
            total += 1
            sentTotal += 1
            if goldTok[2] == predLine[:-1].split('\t')[-1]:
                cor += 1
                sentCor += 1
        else:
            if sentCor != 0:
                sentScores.append(sentCor/sentTotal)
            sentCor = 0 
            sentTotal = 0
    return sentScores, cor/total


for dataset in ['iden', 'trde']:
    ignCaps = False #dataset == 'iden'
    langPred = 'data/' + dataset + '/predicted-bert/test'
    langGold = 'data/' + dataset + '/langFeat/test'
    score = getLangScore(langGold, langPred)

    #normResults
    normGold = 'data/' + dataset + '/norm/test'
    scores = []
    prevScores = []
    for setting in ['lai', 'old.' + dataset + '.' + dataset[2:], 'old.' + dataset + '.' + dataset[:2], 'multiling', 'langaware', 'gold']:
        if setting == 'gold':
            score = 1.0
            sentScores = [True] * len(prevScores)
        else:
            if setting == 'frag':
                goldFragData1 = open('data/' + dataset + '/frags-bert/test.' + dataset[:2]).readlines()
                goldFragData2 = open('data/' + dataset + '/frags-bert/test.' + dataset[2:]).readlines()
                goldFragData = goldFragData1 + goldFragData2
                predData1 = open('preds/test.frag.' + dataset + '.' + dataset[:2] + '.norm').readlines()
                predData2 = open('preds/test.frag.' + dataset + '.' + dataset[2:] + '.norm').readlines()
                predData = predData1 + predData2                
                sentScores, score = getNormScore(goldFragData, predData, ignCaps)
            else:
                if dataset not in setting:
                    setting += '.' + dataset
                sentScores, score = getNormScore(normGold, 'preds/test.' + setting + '.norm', ignCaps)
        scores.append('{:.2f}'.format(score*100))
        if prevScores != []:
            p = signif.getP([prevScores, sentScores])
            if p < .05:
                scores[-1] = '$^*$' + scores[-1]
            #print('{:.2f}'.format(score*100), p)
        prevScores = sentScores
    print(' & '.join([dataset] + scores ) + '\\\\')

#posResults
scores = []
prevScores = []
for setting in ['raw',  'old.tr', 'old.de', 'multiling', 'langaware', 'gold']:
    gold = 'data/trde/pos/' + setting + '.test'
    pred = 'posPreds/test.bert.' + setting + '.out'
    wordScores, score = posEval.eval(gold, pred)
    scores.append('{:.2f}'.format(score*100))
    if prevScores != []:
        p = signif.getP([prevScores, sentScores])
        if p < .05:
            scores[-1] = '$^*$' + scores[-1]
        #print('{:.2f}'.format(score*100), p)
    prevScores = sentScores
print(' & '.join(['trde.pos'] + scores) + '\\\\')



