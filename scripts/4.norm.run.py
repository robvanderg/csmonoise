for langPair in [['id', 'en'], ['tr', 'de']]:
    name = ''.join(langPair)
    for fold in range(10):
        fold = str(fold)
        for setting in ['frags-gold', 'frags-marmot', 'frags-bilty', 'frags-bert']:
            for lang in langPair:
                modelDir = '../../working/' + setting + '.' + name + '.' + lang + '.' + fold
                trainFile = '../../data/' + name + '/' + setting + '/' + lang + '.10fold.train.' + fold
                devFile = '../../data/' + name + '/' + setting + '/' + lang + '.10fold.dev.' + fold
                dataDir = '../../data/' + lang
                outFile = '../../preds/' + setting + '.' + name + '.' + lang + '.out.' + fold
                normFile = '../../preds/' + setting + '.' + name + '.' + lang + '.norm.' + fold
                
                cmd = 'cd monoise/src &&'

                cmd += ' ./tmp/bin/binary -m TR -r ' + modelDir 
                cmd += ' -i ' + trainFile  + ' -d ' + dataDir + ' -C -f 111101111101'
                cmd += ' &> ' + outFile + ' &&'

                cmd += ' ./tmp/bin/binary -m RU -W -r ' + modelDir 
                cmd += ' -i ' + devFile + ' -d ' + dataDir + ' -C -f 111101111101'
                cmd += ' > ' + normFile

                cmd += ' && cd ../../'
                print(cmd)

        for setting in ['multiling', 'langFeat', 'predicted-marmot', 'predicted-bilty', 'predicted-bert']:
            modelDir = '../working/' + setting + '.' + name + '.' + fold
            trainFile = '../data/' + name + '/' + setting + '/10fold.train.' + fold
            devFile = '../data/' + name + '/' + setting + '/10fold.dev.' + fold
            if setting == 'multiling':
                trainFile = trainFile.replace('multiling', 'norm')
                devFile = devFile.replace('multiling', 'norm')
            dataDir1 = '../data/' + langPair[0]
            dataDir2 = '../data/' + langPair[1]
            outFile = '../preds/' + setting + '.' + name + '.out.' + fold
            normFile = '../preds/' + setting + '.' + name + '.norm.' + fold
            cmd = 'cd src &&'

            cmd += ' ./tmp/bin/binary -m TR -r ' + modelDir 
            if setting == 'langFeat' or 'predicted' in setting:
                cmd += ' -E '
            cmd += ' -i ' + trainFile + ' -d ' + dataDir1 + ' -2 ' + dataDir2 
            cmd += ' -C -f 111101111101 &> ' + outFile + '  &&'

            cmd += ' ./tmp/bin/binary -m RU -W -r ' + modelDir 
            if setting == 'langFeat' or 'predicted' in setting:
                cmd += ' -E '
            cmd += ' -i ' + devFile + ' -d ' + dataDir1 + ' -2 ' + dataDir2 
            cmd += ' -C -f 111101111101 > ' + normFile
            
            cmd += ' && cd ../'
            print(cmd)


