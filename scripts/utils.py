def loadNormData(path, ignCaps=False):
    rawData = []
    goldData = []
    langData = []
    curSent = []

    for line in open(path):
        if ignCaps:
            line = line.lower()
        tok = line.strip().split('\t')

        if tok == [''] or tok == []:
            langData.append([x[0] for x in curSent])
            rawData.append([x[1] for x in curSent])
            goldData.append([x[3] for x in curSent])
            curSent = []

        else:
            if len(tok) > 4:
                err('erroneous input, line:\n' + line + '\n in file ' + path + ' contains more then two elements')
            if len(tok) == 3:
                tok.append('')
            curSent.append(tok)

    # in case file does not end with newline
    if curSent != []:
        langData.append([x[0] for x in curSent])
        rawData.append([x[1] for x in curSent])
        goldData.append([x[3] for x in curSent])
    return rawData, goldData, langData

