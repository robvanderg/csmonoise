import os
import utils
import imp
signif = imp.load_source('scripts/bootstrapSign.py', 'scripts/bootstrapSign.py')

def ratio(path):
    raw, gold, _ = utils.loadNormData(path)
    total = 0
    normed = 0
    for rawSent, goldSent in zip(raw, gold):
        for rawWord, goldWord in zip(rawSent, goldSent):
            if rawWord != goldWord:
                normed += 1
            total += 1
    return 1 - (normed/total)

def ratioPerLang(path):
    raw, gold, lang = utils.loadNormData(path)
    total1 = 0
    normed1 = 0
    total2 = 0
    normed2 = 0
    for langSent, rawSent, goldSent in zip(lang, raw, gold):
        for langWord, rawWord, goldWord in zip(langSent, rawSent, goldSent):
            if langWord in ['tr', 'id']:
                if rawWord != goldWord:
                    normed1 += 1
                total1 += 1
            elif langWord in ['de', 'en']:
                if rawWord != goldWord:
                    normed2 += 1
                total2 += 1
    return 1 - (normed1/total1), 1 - (normed2/total2)

def getScore(goldData, predData, verbose=False):
    total = 0
    correct = 0
    sentCor = 0
    sentTotal = 0
    sentScores = []
    for goldLine, predLine in zip(goldData, predData):
        if len(goldLine.strip()) > 1:
            if predLine.strip() == goldLine.strip().split('\t')[-1].strip():
                correct += 1
                sentCor += 1
            elif verbose:
                print(goldLine.strip().split('\t')[-1].strip(), predLine.strip())
            total += 1
            sentTotal += 1
        else:
            sentScores.append(sentCor/sentTotal)
            sentCor = 0
            sentTotal = 0
    return sentScores, correct/total

def get10foldData(path):
    data = []
    for fold in range(10):
        for line in open(path + '.' + str(fold)):
            data.append(line)
    return data

# Not used anymore; but useful for analysis
def getScorePerLang(goldData, predData):
    totalLang1 = 0
    correctLang1 = 0
    totalLang2 = 0
    correctLang2 = 0
    for goldLine, predLine in zip(goldData, predData):
        if len(goldLine.strip()) > 1:
            goldNorm = goldLine.strip().split('\t')[-1].strip()
            lang = goldLine.strip().split('\t')[0].strip()
            if lang in ['tr', 'id']:
                if predLine.strip() == goldLine.strip().split('\t')[-1].strip():
                    correctLang1 += 1
                totalLang1 += 1
            elif lang in ['de', 'en']:
                if predLine.strip() == goldLine.strip().split('\t')[-1].strip():
                    correctLang2 += 1
                totalLang2 += 1
    return correctLang1/totalLang1, correctLang2/totalLang2
    
def getRaw(data):
    newData = []
    for item in data:
        tok = item.split('\t')
        if len(tok) > 1:
            newData.append(tok[1])
        else:
            newData.append('')
    return newData

scores = {}
allSentScores = {}
for langs in [['id', 'en'], ['tr', 'de']]:
    name = ''.join(langs)
    goldData = open('data/' + name + '/langFeat/train').readlines()
    scores[name] = {}
    scores[name][langs[0]] = []
    scores[name][langs[1]] = []


    lai = getRaw(goldData)
    score1, score2 = getScorePerLang(goldData, lai)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)
    
    predData = get10foldData('preds/mfr.' + name + '.norm')
    score1, score2 = getScorePerLang(goldData, predData)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)
    
    predData1 = get10foldData('preds/old.' + name + '.' + langs[0] + '.norm')
    predData2 = get10foldData('preds/old.' + name + '.' + langs[1] + '.norm')
    score1, score2 = getScorePerLang(goldData, predData1)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)
    score1, score2 = getScorePerLang(goldData, predData2)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)

    goldFragData1 = [langs[0] + '\t' + line for line in open('data/' + name + '/frags-bert/train.' + langs[0]).readlines()]
    goldFragData2 = [langs[1] + '\t' + line for line in open('data/' + name + '/frags-bert/train.' + langs[1]).readlines()]
    goldFragData = goldFragData1 + goldFragData2
    predData1 = get10foldData('preds/frags-bert.' + name + '.' + langs[0] + '.norm')
    predData2 = get10foldData('preds/frags-bert.' + name + '.' + langs[1] + '.norm')
    predData = predData1 + predData2

    score1, score2 = getScorePerLang(goldFragData, predData)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)

    predData = get10foldData('preds/multiling.' + name + '.norm')
    score1, score2 = getScorePerLang(goldData, predData)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)
    
    predData = get10foldData('preds/predicted-bert.' + name + '.norm')
    score1, score2 = getScorePerLang(goldData, predData)
    scores[name][langs[0]].append(score1)
    scores[name][langs[1]].append(score2)
    
print('Model & id & en & tr & de \\\\')
names = ['LAI', 'MFR', 'Monolingual-lang1 (tr/id)', 'Monolingual-lang2 (de/en)', 'Frags', 'Multilingual', 'Language-aware']
for modelIdx in range(len(scores['iden']['id'])):
    row = [names[modelIdx]]
    for langs in [['id', 'en'], ['tr', 'de']]:
        name = ''.join(langs)
        for lang in langs:
            row.append('{:.2f}'.format(scores[name][lang][modelIdx]*100))
    print(' & '.join(row) + ' \\\\')
print()

         


