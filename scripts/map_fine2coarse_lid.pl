# ozlem - 11.05.20
# maps fine-grained LID to coarse-grained LID
#
# fine-grain labels are the original tag set of the tr-de twitter data 
# coarse-grain labels are language1, language2, and undecided ('un' comes from the id-en data set)
# The input and output format is 4-col files used in lid experiments (rob has prepared them)
# lid\torig\tPLACEHOLDER\tgold
#
# usage:
# perl scripts/util/map_fine2coarse_lid.pl < fineGrained/all > coarseGrained/all
#
# on all files in fineGrained:
# for file in *; perl map_fine2coarse_lid.pl < $file >../coarseGrained/$file ; done



use strict;

use utf8;	# utf8 in the code
use open ':encoding(utf8)';
binmode STDERR,":encoding(utf8)";
binmode STDOUT,":encoding(utf8)";
binmode STDIN,":encoding(utf8)";


my %mapping = (
TR => 'tr',
DE => 'de',
MIXED => 'de',
AMBIG => 'un',
LANG3 => 'un',
OTHER => 'un'
);


while (<>){
	if (/^$/){
		print;
	}
	else{
		s/NE\.//;
		s/(DE|TR|MIXED|AMBIG|OTHER|LANG3)\t/$mapping{$1}\t/;
		print;
	}
}

