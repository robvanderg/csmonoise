import os
from pprint import pprint

data = {'trde': {'word_idx':0, 'tasks': {'pos': {'column_idx':1, 'task_type':'seq'}}}}


data['trde']['train_data_path'] = '../data/trde/pos/ud-tr-de.train'
data['trde']['validation_data_path'] = '../data/trde/pos/ud-tr-de.dev'
data['trde']['copy_other_columns'] = True
jsonPath = 'config/pos.json'
with open(jsonPath, 'wt') as out:
    pprint(data, stream=out)
os.system( "sed -i \"s;True;true;g\" " + jsonPath)
cmd = 'cd mtp && python3 train.py --parameters_config configs/params.json --dataset_config ../' + jsonPath + ' --name pos --device 0 && cd ../'
print(cmd)

for setting in ['raw', 'multiling', 'langaware', 'gold']:
    inp = '../data/trde/pos/' + setting + '.dev.txt'
    output = '../posPreds/bert.' + setting + '.out'
    cmd = 'cd mtp && python3 predict.py logs/pos/*/model.tar.gz ' + inp + ' ' + output + ' --device 0'
    print(cmd)
    
