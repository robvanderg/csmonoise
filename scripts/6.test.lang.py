from pprint import pprint
import os

if not os.path.isdir('config'):
    os.mkdir('config')

cmd = 'python3 scripts/0.prep.convLangs.py data/trde/langFeat/test' 
print(cmd)

datasets = ['trde', 'iden']
for dataset in datasets:
    data = {dataset: {'word_idx':1, 'tasks': {'langId': {'column_idx':0, 'task_type': 'seq'}}}}
    data[dataset]['train_data_path'] = '../data/' + dataset + '/langFeat/train'
    data[dataset]['validation_data_path'] = '../data/' + dataset + '/langFeat/test'
    data[dataset]['test_data_path'] = '../data/' + dataset + '/langFeat/test'
    data[dataset]['copy_other_columns'] = True

    jsonPath = 'config/' + dataset + '.json'
    with open(jsonPath, 'wt') as out:
        pprint(data, stream=out)
    os.system( "sed -i \"s;True;true;g\" " + jsonPath)
    cmd = 'cd mtp && python3 train.py --parameters_config configs/params.json --dataset_config ../' + jsonPath + ' --name ' + dataset + ' --device 0 && cd ../ ' 
    cmd += ' && cat mtp/logs/' + dataset + '/*/*dev.out > data/' + dataset + '/predicted-bert/test'
    print(cmd)
