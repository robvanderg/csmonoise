from pprint import pprint

for setting in ['raw', 'mfr', 'old.tr', 'old.de', 'multiling', 'langaware', 'gold']:
    inp = '../data/trde/pos/' + setting + '.test.txt'
    output = '../posPreds/test.bert.' + setting + '.out'
    cmd = 'cd mtp && python3 predict.py logs/pos/*/model.tar.gz ' + inp + ' ' + output + ' --device 0'
    print(cmd)

