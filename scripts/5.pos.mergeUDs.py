import sys
import random

if len(sys.argv) < 4:
    print('please provide two treebanks and an output path')
    exit(1)

def readConll(path):
    allData = []
    curSent = []
    for line in open(path):
        tok = line.strip().split('\t')
        if line[0] == '#':
            continue
        elif len(tok) == 10:
            if tok[0].isdigit():
                curSent.append(tok)
        else:
            allData.append(curSent)
            curSent = []
    return allData

data1 = readConll(sys.argv[1])
data2 = readConll(sys.argv[2])

random.seed(8446)
random.shuffle(data1)
random.shuffle(data2)

minSize = min(len(data1), len(data2))
allData = data1[:minSize] + data2[:minSize]
random.shuffle(allData)

outFile = open(sys.argv[3], 'w')
for sent in allData:
    for tokLine in sent:
        outFile.write(tokLine[1] + '\t' + tokLine[3] + '\n')
    outFile.write('\n')
outFile.close()

