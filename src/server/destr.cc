#include "server.ih"

Server::~Server()
{
     close(d_clientSockFD);
     close(d_serverSockFD);
}
