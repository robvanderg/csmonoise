#include "server.ih"
// based on: 
// https://stackoverflow.com/questions/2064636/getting-the-source-address-of-an-incoming-socket-connection

string Server::getInput()
{
    listen(d_serverSockFD,5);
    socklen_t clilen = sizeof(cli_addr);
    
    d_clientSockFD = accept(d_serverSockFD, (struct sockaddr *) &cli_addr, &clilen);
   
    char ipstr[INET6_ADDRSTRLEN]; 
    if (!checkIp(&ipstr[0]))
    {
        cout << "Warning, someone is trying to connect from: " 
            << ipstr  << "\n";
        return "ERROR: it is not allowed to normalize from ip: " + string(ipstr) + "\nPlease report this bug: r.van.der.goot@rug.nl";
    }

    if (d_clientSockFD < 0)
    {
        cout << "Warning, d_clientSockFD < 0\n";
        return "ERROR: connection not accepted\nPlease report this bug: r.van.der.goot@rug.nl";
    }

    char buffer[d_bufferSize];
    bzero(buffer, d_bufferSize);
    int got = read(d_clientSockFD, buffer, d_bufferSize); 
    if (got < 0) 
    {
        cout << "Warning, coult not read input\n";
        return "ERROR: reading from socket failed\nPlease report this bug: r.van.der.goot@rug.nl";
    }
    return string(buffer);
}
