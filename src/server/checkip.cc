#include "server.ih"

bool Server::checkIp(char *ipstr)
{
    if (cli_addr.ss_family == AF_INET) { //IPv4
        struct sockaddr_in *s = (struct sockaddr_in *)&cli_addr;
        inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
    } else { // IPv6
        struct sockaddr_in6 *s = (struct sockaddr_in6 *)&cli_addr;
        inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
    }

    return (strcmp(ipstr, "129.125.14.93") != 0);
}
