#include "server.ih"

// converts value between 0-50 to 26-1 and 50-100 1-0
double Server::getWeight(std::string const &weightStr)
{
    for (size_t beg = 0; beg != weightStr.size(); ++beg)
        if (!isdigit(weightStr[beg]))
            return 1.0;

    int weight = stoi(weightStr);
    
    if (weight < 0)
        return 1.0;
    else if (weight <= 50) // 50->1 1-> 10.8
        return (55-weight) / 5;
    else if (weight > 50 && weight < 75) // 51->0.97 75->0.25
        return .03 * (75 - weight ) + .25 ;
    else if (weight < 100) // 75->0.25 99->0.05
        return .008 * (99 - weight ) + .05 ;
    else if (weight == 100)
        return 0.01;
    return 1.0;
}
