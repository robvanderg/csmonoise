mkdir -p tmp/bin
mkdir -p tmp/o

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11split.o server/split.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11server1.o server/server1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11sendback.o server/sendback.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11run.o server/run.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11normalize.o server/normalize.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11handlenorm.o server/handlenorm.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11getweight.o server/getweight.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11getlangidx.o server/getlangidx.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11getinput.o server/getinput.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11destr.o server/destr.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/11checkip.o server/checkip.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/10readconfig.o config/readconfig.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9writefeats.o model/writefeats.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9write.o model/write.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9train3.o model/train3.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9train2.o model/train2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9train.o model/train.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9tolower.o model/tolower.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9tokenize.o model/tokenize.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9tofsa.o model/tofsa.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9test.o model/test.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9strip.o model/strip.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9splitstring.o model/splitstring.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9split.o model/split.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9sortcands.o model/sortcands.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9run2.o model/run2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9run.o model/run.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9readraw.o model/readraw.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9readgold.o model/readgold.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9rank.o model/rank.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9model1.o model/model1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9match.o model/match.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9loadforest2.o model/loadforest2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9loadforest.o model/loadforest.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9kfold.o model/kfold.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9isword.o model/isword.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9interactive.o model/interactive.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9genlookup.o model/genlookup.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9gen.o model/gen.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9errdet.o model/errdet.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9consider.o model/consider.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9addngrams.o model/addngrams.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9addgold.o model/addgold.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/9addcands.o model/addcands.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/8save.o lookup/save.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/8lookup1.o lookup/lookup1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/8load.o lookup/load.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/8iscanon.o lookup/iscanon.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/8getlookups.o lookup/getlookups.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/8contains.o lookup/contains.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/8addword.o lookup/addword.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/7w2v2.o w2v/w2v2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/7w2v1.o w2v/w2v1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/7printvocab.o w2v/printvocab.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/7load.o w2v/load.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/7getid.o w2v/getid.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/7find.o w2v/find.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/7distance.o w2v/distance.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/6utility.o ranger/utility.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/6TreeProbability.o ranger/TreeProbability.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/6Tree.o ranger/Tree.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/6ForestProbability.o ranger/ForestProbability.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/6Forest.o ranger/Forest.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/6DataDouble.o ranger/DataDouble.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/6Data.o ranger/Data.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/6ArgumentHandler.o ranger/ArgumentHandler.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/5printwordlist.o aspgen/printwordlist.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/5find2.o aspgen/find2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/5find1.o aspgen/find1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/5destr.o aspgen/destr.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/5aspgen1.o aspgen/aspgen1.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/4savetxt.o embeds/savetxt.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/4savebin.o embeds/savebin.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/4loadtxt.o embeds/loadtxt.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/4loadbin.o embeds/loadbin.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/4getdistance.o embeds/getdistance.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/4find.o embeds/find.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/4embeds2.o embeds/embeds2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/4embeds1.o embeds/embeds1.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3savebin.o ngram/savebin.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3save.o ngram/save.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3readuni.o ngram/readuni.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3readbi.o ngram/readbi.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3ngram2.o ngram/ngram2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3ngram1.o ngram/ngram1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3loadbin.o ngram/loadbin.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3load.o ngram/load.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3learn.o ngram/learn.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3gettotalcount.o ngram/gettotalcount.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3gettopn.o ngram/gettopn.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3getcount2.o ngram/getcount2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3getcount1.o ngram/getcount1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/3findbigram.o ngram/findbigram.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2vocab3.o vocab/vocab3.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2vocab2.o vocab/vocab2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2vocab1.o vocab/vocab1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2savebin2.o vocab/savebin2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2savebin1.o vocab/savebin1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2save2.o vocab/save2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2save1.o vocab/save1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2optimize.o vocab/optimize.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2loadordered2.o vocab/loadordered2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2loadordered1.o vocab/loadordered1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2loadbin2.o vocab/loadbin2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2loadbin1.o vocab/loadbin1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2load2.o vocab/load2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2load1.o vocab/load1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2getword.o vocab/getword.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2getrange.o vocab/getrange.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2getid2.o vocab/getid2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2getid1.o vocab/getid1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2findbeg.o vocab/findbeg.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2contains.o vocab/contains.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2clean.o vocab/clean.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2addword.o vocab/addword.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/2addordered.o vocab/addordered.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/1recall.o eval/recall.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/1print.o eval/print.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/1f1.o eval/f1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/1evalsent.o eval/evalsent.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/1eval1.o eval/eval1.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/1errorrates.o eval/errorrates.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/1errordetection.o eval/errordetection.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/1adddistance2.o eval/adddistance2.cc
g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/1adddistance1.o eval/adddistance1.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/o/0server.o ./server.cc

g++ --std=c++11 -Wall -g -O2 -c -o tmp/main.o main.cc

cd tmp
g++ --std=c++11 -Wall -g -O2 -o bin/binary main.o o/*.o -lpthread -L./../aspgen -Wl,-rpath=./aspgen -laspell 

