#include "ngram.ih"

void NGram::readBi(string path, uint32_t minCount)
{
    map<uint64_t, uint32_t> bigramCounts;
    uint32_t begId = d_unigrams.getId(d_beg.c_str());
    uint32_t endId = d_unigrams.getId(d_end.c_str());
    string line;
    ifstream in(path);
    if (!in.good())
    {
        cerr << "Could not read bigrams " << path << '\n';
        exit(1);
    }
    while(getline(in, line))
    {
        uint32_t prevId = begId;
        istringstream iss(line);
        string curWord;
        while(iss >> curWord)
        {
            uint32_t curId = d_unigrams.getId(curWord.c_str());
            if (prevId != 0 && curId != 0)
            {
                uint64_t bigramId = prevId;
                bigramId <<= 32;
                bigramId += curId;

                auto got = bigramCounts.find(bigramId);
                if (got == bigramCounts.end())
                    bigramCounts.emplace(bigramId, 1);
                else
                    ++got->second;
            }
            prevId = curId;
        }
        if (prevId != 0)
        {
            uint64_t bigramId = prevId; // For last word in sentence + <\S>
            bigramId <<= 32;
            bigramId += endId;
            auto got = bigramCounts.find(bigramId);
            if (got == bigramCounts.end())
                bigramCounts.emplace(bigramId, 1);
            else
                ++got->second;
        }
    }
    in.close();
    for (std::map<uint64_t, uint32_t>::iterator it=bigramCounts.begin(); it!=bigramCounts.end(); ++it)
    {
        if (it->second >= minCount)
        {
            d_bigrams.push_back(it->first);
            d_bigramCounts.push_back(it->second);
        }
    }
}
