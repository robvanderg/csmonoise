#include "ngram.ih"

vector<pair<uint64_t, string>> NGram::getTopN(int n)
{
    vector<pair<uint64_t, string>> contents;
    for (size_t beg = 0; beg != d_unigramCounts.size(); ++beg)
    {
        if (d_unigrams.getWord(beg)[0] != '@' && d_unigrams.getWord(beg)[0] != '#')
            contents.push_back(make_pair(d_unigramCounts[beg], d_unigrams.getWord(beg)));
    }
    sort(contents.rbegin(), contents.rend()); // DESCENDING!
    contents.resize(n);
    return contents;
}
