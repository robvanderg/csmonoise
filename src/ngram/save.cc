#include "ngram.ih"

void NGram::save(string path)
{
    cerr << "Writing: " << path << ".1\n";
    ofstream uniFile(path + ".1");
    if (!uniFile.good())
    {
        cerr << "Could not write unigrams: " << path << ".1\n";
        exit(1);
    }
    for (size_t unigrIdx = 1; unigrIdx != d_unigramCounts.size(); ++unigrIdx)
        uniFile << d_unigrams.getWord(unigrIdx) << '\t' << d_unigramCounts[unigrIdx] << '\n';
    uniFile.close();
    
    cerr << "Writing: " << path << ".2\n";
    ofstream biFile(path + ".2");
    if (!biFile.good())
    {
        cerr << "Could not write bigrams: " << path << ".2\n";
        exit(1);
    }
    for (size_t bigrIdx = 1; bigrIdx != d_bigrams.size(); ++bigrIdx)
    {
        uint64_t bigrId = d_bigrams[bigrIdx];
        uint64_t word1 = bigrId;
        word1 >>= 32;

        uint64_t diff = word1;
        diff <<= 32;
        uint64_t word2 = bigrId - diff;
        
        biFile << d_unigrams.getWord(word1) << ' ' << d_unigrams.getWord(word2)
               << '\t' << d_bigramCounts[bigrIdx] << '\n';
    }
    biFile.close();
}

