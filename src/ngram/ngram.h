#ifndef INCLUDED_NGRAM_
#define INCLUDED_NGRAM_

#include <string>
#include <vector>
#include <utility> //pair
#include <stdint.h>
#include <iostream>

#include "../vocab/vocab.h"

class NGram
{
    Vocab d_unigrams;
    std::vector<uint64_t> d_unigramCounts;
    
    std::vector<uint64_t> d_bigrams;
    std::vector<uint32_t> d_bigramCounts;
 
    std::string d_beg = "<S>";
    std::string d_end = "</S>";

    uint64_t d_totalCount = 0;

    char *d_searchWord;
 
    public:
        NGram();
        NGram(std::string path, bool bin = false);
        
        void learn(std::string path, uint32_t minCount);
        void save(std::string path);
        void load(std::string path);
        void saveBin(std::string path);
        void loadBin(std::string path);

        // should be pointers?
        uint64_t getCount(std::string word);
        uint32_t getCount(std::string word1, std::string word2);

        Vocab *getVocab(){return &d_unigrams;};
        uint64_t getTotalcount();
        std::vector<std::pair<uint64_t, std::string>> getTopN(int n);

    private:
        void readUni(std::string path, uint32_t minCount);
        void readBi(std::string path, uint32_t minCount);
        
        uint32_t findBigram(uint64_t id, uint32_t beg, uint32_t end);
};

#endif
