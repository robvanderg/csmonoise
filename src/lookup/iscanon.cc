#include "lookup.ih"

bool Lookup::isCanon(string const &word)
{
    return d_knowns.contains(word);
}
