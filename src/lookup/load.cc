#include "lookup.ih"

void Lookup::load(string const &path)
{
    ifstream ifs(path);
    if (!ifs.good())
    {
        cerr << "Could not read lookup list: " << path << '\n';
    }

    string line;
    map<string, map<string, double>>::iterator curSrc;

    while (getline(ifs, line))
    {
        if (line[0] != ' ')
            curSrc = d_data.emplace(line, map<string, double>()).first;
        else
        {
            istringstream iss(line);
            string cand, word;
            double count;
            iss >> count >> cand;
            while (iss >> word)
                cand += ' ' + word;
            curSrc->second[cand] = count; 
            d_knowns.addWord(cand);
        }
    }
    d_knowns.optimize();
}
