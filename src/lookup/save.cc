#include "lookup.ih"

void Lookup::save(string const &path)
{
    ofstream ofs(path);
    if (!ofs.good())
    {
        cerr << "Could not write lookup list: " << path;
        exit(1);
    }
    for (auto const &src: d_data)
    {
        ofs << src.first << '\n';
        for (auto const &tgt: src.second)
            ofs << ' ' << tgt.second << ' ' << tgt.first << '\n';
    }
    ofs.close();
}
