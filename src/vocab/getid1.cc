#include "vocab.ih"

uint32_t Vocab::getId(string const &target)
{
    return getId(0, d_idxs.size()-1, &target[0]);
}
