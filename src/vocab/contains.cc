#include "vocab.ih"

bool Vocab::contains(string const &target)
{
    return getId(target) != 0; 
}
