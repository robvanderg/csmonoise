#include "vocab.ih"

uint32_t Vocab::findBeg(uint32_t beg, uint32_t end, char const *searchWord)
{
    uint32_t split = (end + beg) / 2;
    uint32_t lenSplit = strlen(&d_vocab[d_idxs[split]]);
    uint32_t lenSearchword = strlen(searchWord);

    if (lenSplit > lenSearchword)
        lenSplit = lenSearchword;

    int comp = strncmp(searchWord, &d_vocab[d_idxs[split]], lenSplit);
    int comp2 = strncmp(searchWord, &d_vocab[d_idxs[split]], lenSearchword);
    if (comp2 == 0)
        return split;
    if (lenSplit < lenSearchword)
    {
        comp = strcmp(searchWord, &d_vocab[d_idxs[split]]);
    }
    if (beg == end || beg == split)
        return 0;
    if (comp > 0)
        return findBeg(split, end, searchWord);
    if (comp < 0)
        return findBeg(beg, split, searchWord);
    return split;
}
