#include "vocab.ih"

void Vocab::save(ofstream *ofs)
{
    if (d_idxs.size() <= 1)
        return;
    for (uint32_t wordIdx = 1; wordIdx != d_idxs.size(); ++wordIdx)
    {
        (*ofs) << &d_vocab[d_idxs[wordIdx]] << '\n';
    }
}
