#include "vocab.ih"

void Vocab::loadOrdered(string const &path)
{
    cerr << "Loading: " << path;
    if (path == "")
        return;
    ifstream in(path);
    if (!in.good())
    {
        cerr << "Could not read vocab: " << path << '\n';
        exit(1);
    }
    loadOrdered(&in);
}
