#include "vocab.ih"

pair<uint32_t, uint32_t> Vocab::getRange(char const *word)
{
    uint32_t beg = findBeg(0, d_idxs.size()-1, word);
    if (beg == 0)
        return make_pair(0,0);
    
    size_t end = beg;
    for (;beg != 0; --beg)
        if (strncmp(word, &d_vocab[d_idxs[beg]], strlen(word))!= 0)
            break;
    
    for (; end != d_idxs.size(); ++end)
        if(strncmp(word, &d_vocab[d_idxs[end]], strlen(word))!= 0)
            break;

    return make_pair(beg + 1, end);
}
