#include "config/main.ih"
#include "server/server.h"

void server()
{
    std::vector<Model*> models;
    std::vector<std::string> langs;
    
    // English
    std::vector<std::string> arguments = {"-r", "../data/en/chenli", "-d", "../data/en", "-t", "-C", "-b"};
    std::vector<char*> argv2;
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    option::Stats  stats(usage, argv2.size()-1, argv2.data());
    std::vector<option::Option> options(stats.options_max);
    std::vector<option::Option> buffer(stats.buffer_max);
    option::Parser parse(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig;
    readConfig(&myConfig, options);
    Model myModel(&myConfig);
    myModel.loadForest();
    models.push_back(&myModel);
    langs.push_back("en");

    // Spanish
    arguments[1] = "../data/es/model";
    arguments[3] = "../data/es";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig2;
    readConfig(&myConfig2, options);
    Model myModel2(&myConfig2);
    myModel2.loadForest();
    models.push_back(&myModel2);
    langs.push_back("es");
    
    // Turkish
    arguments[1] = "../data/tr/model";
    arguments[3] = "../data/tr";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig3;
    readConfig(&myConfig3, options);
    Model myModel3(&myConfig3);
    myModel3.loadForest();
    models.push_back(&myModel3);
    langs.push_back("tr");
    
    // Italian
    arguments[1] = "../data/it/model";
    arguments[3] = "../data/it";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig8;
    readConfig(&myConfig8, options);
    Model myModel8(&myConfig8);
    myModel8.loadForest();
    models.push_back(&myModel8);
    langs.push_back("it");
    
    arguments.pop_back(); //no bad model!
    // Croatian
    arguments[1] = "../data/hr/model";
    arguments[3] = "../data/hr";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig4;
    readConfig(&myConfig4, options);
    Model myModel4(&myConfig4);
    myModel4.loadForest();
    models.push_back(&myModel4);
    langs.push_back("hr");
    
    // Dutch
    arguments[1] = "../data/nl/model";
    arguments[3] = "../data/nl";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig5;
    readConfig(&myConfig5, options);
    Model myModel5(&myConfig5);
    myModel5.loadForest();
    models.push_back(&myModel5);
    langs.push_back("nl");

    // Serbian
    arguments[1] = "../data/sr/model";
    arguments[3] = "../data/sr";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig6;
    readConfig(&myConfig6, options);
    Model myModel6(&myConfig6);
    myModel6.loadForest();
    models.push_back(&myModel6);
    langs.push_back("sr");
    
    // Slovenian
    arguments[1] = "../data/sl/model";
    arguments[3] = "../data/sl";
    argv2 = std::vector<char*>();
    for (const auto& arg : arguments)
        argv2.push_back((char*)arg.data());
    argv2.push_back(nullptr);
    stats = option::Stats(usage, argv2.size()-1, argv2.data());
    options = std::vector<option::Option>(stats.options_max);
    buffer = std::vector<option::Option>(stats.buffer_max);
    parse = option::Parser(usage, argv2.size()-1, argv2.data(), &options[0], &buffer[0]);
    Config myConfig7;
    readConfig(&myConfig7, options);
    Model myModel7(&myConfig7);
    myModel7.loadForest();
    models.push_back(&myModel7);
    langs.push_back("sl");

    Server server(langs, models, 8446);
    std::cout << "loaded all\n";
    server.run();
}
