#include "aspgen.ih"

void AspGen::print_word_list(AspellSpeller * speller,
                const AspellWordList *wl,
                char delem)
{
  if (wl == 0) {
    printf("Error: %s\n", aspell_speller_error_message(speller));
  } else {
    AspellStringEnumeration * els = aspell_word_list_elements(wl);
    const char * word;
    while ( (word = aspell_string_enumeration_next(els)) != 0) {
      fputs(word, stdout);
      putc(delem, stdout);
    }
    delete_aspell_string_enumeration(els);
  }
}

