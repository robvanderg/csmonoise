#include "eval.ih"

void Eval::addDistance(string &word1, string &word2)
{
    const std::size_t len1 = word1.size(), len2 = word2.size();
    std::vector<size_t> col(len2+1), prevCol(len2+1);
    
    for (size_t i = 0; i < prevCol.size(); i++)
        prevCol[i] = i;
    for (size_t i = 0; i < len1; i++) 
    {
        col[0] = i+1;
        for (size_t j = 0; j < len2; j++)
            col[j+1] = std::min({ prevCol[1 + j] + 1, col[j] + 1, 
                                prevCol[j] + (word1[i]==word2[j] ? 0 : 1) });
        col.swap(prevCol);
    }

    d_charDistance += prevCol[len2];
    d_charTotal += len2;
}
