#include "eval.ih"

void Eval::f1(vector<string> cands)
{
    // For precision recall and F1 score
    bool needNorm = d_origs[d_wordIdx] != d_cors[d_wordIdx];
    bool normed = d_bestSeq[d_wordIdx] != d_origs[d_wordIdx];
    bool cor = d_bestSeq[d_wordIdx] == d_cors[d_wordIdx];

    if (needNorm)
    {
        if (cor)
            d_TP += 1;
        else
        {
            /*if (normed)
                d_FP += 1;
            else*/
            d_FN += 1;
        }
    }
    else 
    {
        if (cor) 
            d_TN += 1;
        else
            d_FP += 1;
    }
    if (normed)
        ++d_totalNormed;
    if (needNorm)
        ++d_needNorm;
}
