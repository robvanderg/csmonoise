#include "eval.ih"

void Eval::errorDetection()
{
    bool normed = d_origs[d_wordIdx] != d_cors[d_wordIdx];
    bool guess = d_bestSeq[d_wordIdx] != d_origs[d_wordIdx];
    if (guess)
    {
        if (normed)
            d_detTP += 1;
        else
            d_detFP += 1;
    }
    else
    {
        if (normed)
            d_detFN += 1;
        else
            d_detTN += 1;
    }
}
