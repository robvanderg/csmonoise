#include "eval.ih"

void Eval::recall(vector<string> cands)
{
    if (cands.empty())
    {
        if (d_origs[d_wordIdx] == d_cors[d_wordIdx])
            ++d_recall[0];
        else
        {
            ++d_notFound;
            if (d_verbose)
                cout << "NN: " << d_origs[d_wordIdx] << '\t' << d_cors[d_wordIdx] <<'\n';
        }
        return;
    }

    bool found = false;
    size_t corIdx = 0;
    for (; corIdx != cands.size(); ++corIdx)
    {
        if (cands[corIdx] == d_cors[d_wordIdx])
        {
            ++d_recall[corIdx];
            found = true;
            if (d_verbose && corIdx != 0)
            {
                cout << "WR: " << d_origs[d_wordIdx] << " -> " << d_cors[d_wordIdx]
                     << '\t' << cands.size() << '\t' << corIdx << '\n';
                for (size_t beg = 0; beg != corIdx+1; ++beg)
                    cout << ' ' << beg << ". " << cands[beg] << '\n';
            }
            break;
        }
    }

    if (corIdx == 1)
    {
        size_t beg = 0;
        for (; beg != cands.size(); ++ beg)
            if (cands[beg] == d_origs[d_wordIdx])
                break;
        if (beg == 0)
            d_origPos1 += 1;
        else if (beg == 1)
            d_origPos2 += 1;
        else
            d_origPosOther += 1;
    }
    
    /*if (cands.back() == d_cors[d_wordIdx] && cands.size() > 1)
    {
        if (cands.size() == 2)
            cout << cands[0] << '\t' << cands[1] << '\n';
        cout << '\t' << cands.size() << '\t' << d_origs[d_wordIdx] << '\t' <<d_cors[d_wordIdx] << '\n';
    }*/

    if (!found)
    {
        if (d_verbose)
            cout << "NF: " << d_origs[d_wordIdx] << '\t' << d_cors[d_wordIdx]
                 << '\t' << cands.size() << '\n';
        ++d_notFound;
    }
}
