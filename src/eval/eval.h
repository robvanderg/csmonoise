#ifndef INCLUDED_EVAL_
#define INCLUDED_EVAL_

#include <string>
#include <vector>
#include <ostream>

class Eval
{
    size_t d_wordIdx = 0;
    bool d_verbose = false;
    bool d_goldErrDet = false;
    bool d_goldErrDet2 = false;
    std::vector<std::string> d_bestSeq; 
    std::vector<std::string> d_cors; 
    std::vector<std::string> d_origs; 

    // CER/WER
    size_t d_charDistance = 0;
    size_t d_charTotal = 0;
    size_t d_wordDistance = 0;
    size_t d_wordTotal = 0;

    // F1
    size_t d_TP = 0;
    size_t d_FP = 0;
    size_t d_FN = 0;
    size_t d_TN = 0;
    size_t d_totalNormed = 0;
    size_t d_needNorm = 0;

    // Error Detection
    size_t d_detTP = 0;
    size_t d_detFP = 0;
    size_t d_detFN = 0;
    size_t d_detTN = 0;
   
    std::vector<size_t> d_recall;
    size_t d_notFound = 0;
    size_t d_totalWords = 0;
    size_t d_totalCands = 0;
    
    // Duration
    size_t trainSecs = 0;
    size_t testSecs = 0;

    // Extra
    size_t d_origPos1 = 0;
    size_t d_origPos2 = 0;
    size_t d_origPosOther = 0;

    public:
        Eval(bool verbose, bool goldErrDet, bool goldErrDet2);

        void evalSent(std::vector<std::vector<std::string>> cands, std::vector<std::string> orig, std::vector<std::string> gold, std::vector<bool> oov);
        
        void setTrainSecs(size_t secs){trainSecs = secs;};
        void setTestSecs(size_t secs){testSecs = secs;};

        void print(std::ostream *out);

    private:
        void addDistance(std::string &word1, std::string &word2);
        void addDistance(std::string *sent1, size_t size1, std::string *sent2, size_t size2);

        void errorRates();
        void errorDetection();
        void recall(std::vector<std::string> cands);
        void f1(std::vector<std::string> cands);
};
        
#endif
