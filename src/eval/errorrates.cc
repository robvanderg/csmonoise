#include "eval.ih"

void Eval::errorRates()
{
    string bestSeq;
    for (string word: d_bestSeq)
        bestSeq += word + " ";
    string cor;
    for (string word: d_cors)
        cor += word + " ";
    addDistance(bestSeq, cor);
    addDistance(&d_bestSeq[0], d_bestSeq.size(), &d_cors[0], d_cors.size());
}
