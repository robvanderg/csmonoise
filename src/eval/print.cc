#include "eval.ih"

void Eval::print(ostream *out)
{
    double precDet = double(d_detTP) / (d_detTP + d_detFP);
    double recDet = double(d_detTP) / (d_detTP + d_detFN);
    double f1Det = 2 * (precDet * recDet) / (precDet + recDet);

    double prec = double(d_TP) / d_totalNormed;
    double rec = double(d_TP)/ d_needNorm;
    double f1 = 2 * (prec * rec) / (prec + rec);

    (*out) << "EVALUATION:\n";

    (*out) << setw(5) << "rank" << setw(10) << "counts" << "    " << "recall\n";
    size_t cumulat = 0;
    for (size_t beg = 0; beg != 10; ++beg)
    {
        cumulat += d_recall[beg];
        (*out) << setw(5) << beg+1 << setw(10) << d_recall[beg] << "    " 
            << setprecision(4) << (cumulat/double(d_totalWords)) << '\n';
    }
    (*out) << '\n';

    (*out) << "                  " << setw(7) << "rec" << setw(7) << "prec" << setw(7) << "f1\n";
    (*out) << "Error Detection:  "
           << setw(7) << setprecision(4) << recDet 
           << setw(7) << setprecision(4) << precDet 
           << setw(7) << setprecision(4) << f1Det << '\n';
    (*out) << "Normalization:    "
           << setw(7) << setprecision(4) << rec 
           << setw(7) << setprecision(4) << prec 
           << setw(7) << setprecision(4) << f1 << '\n' << '\n';

    (*out) << "WER:  " <<  setprecision(4)
           << (d_wordDistance == 0? 0.0: double(d_wordDistance)/d_wordTotal)
           << '\n'; 
    (*out) << "CER:  " << setprecision(4)
           << (d_charDistance == 0? 0.0: double(d_charDistance)/d_charTotal)
           << '\n' << '\n';

    (*out) << "notFound:           " << d_notFound << '\n';
    (*out) << "Total:              " << d_totalWords << '\n';
    (*out) << "Average candidates: " << setprecision(4) 
            << double(d_totalCands) / d_totalWords << '\n';
    (*out) << "Upperbound:         " << setprecision(4) 
            << (double(d_totalWords - d_notFound) / d_totalWords) 
            << " (" << d_totalWords -d_notFound << ")\n";
    (*out) << "Baseline:           " << 1 - (double(d_needNorm) / d_totalWords)
            << " (" << d_totalWords - d_needNorm << ")\n";
    if (trainSecs != 0)
        (*out) << "Training time:      " << trainSecs << '\n';
    (*out) << "Testing time:       " << testSecs << '\n' << '\n';
    (*out) << "TP " << d_TP << '\n';
    (*out) << "TN " << d_TN << '\n';
    (*out) << "FP " << d_FP << '\n';
    (*out) << "FN " << d_FN << '\n';
    (*out) << "Real Prec: " << d_TP / double(d_TP + d_FP) << '\n';
    (*out) << "normed: " << d_totalNormed << '\n';
    (*out) << "Real rec: " << d_TP / double(d_TP + d_FN) << '\n';
    (*out) << "needNorm: " << d_needNorm << '\n';
    (*out) << "ERR:  " << (d_TP - d_FP) / double(d_TP + d_FN) << '\n';;

    (*out) << '\n';
    (*out) << "orig = cand[0]     " << d_origPos1 << '\n';
    (*out) << "orig = cand[1]     " << d_origPos2 << '\n';
    (*out) << "orig = cand[other] " << d_origPosOther << '\n';
    //(*out) << double(d_TP) / d_totalNormed << '\n';
}

