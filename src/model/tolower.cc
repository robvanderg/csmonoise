#include "model.ih"

string Model::toLower(const string &src)
{
    try
    {
        // This function also lowercases diacritis: ie. Ũ 
        wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
        wstring wstr = converter.from_bytes(src);

        //locale::global(locale(""));
        std::locale mylocale("");
        std::wcout.imbue(mylocale);
        auto& f = use_facet<ctype<wchar_t>>(mylocale);

        f.tolower(&wstr[0], &wstr[0] + wstr.size());
        string result = converter.to_bytes(wstr);
        return result;
    }
    catch (...) {
        string result = src;
        transform(result.begin(), result.end(), result.begin(), ::tolower);
        return result;
    }
}

