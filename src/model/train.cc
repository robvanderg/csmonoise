#include "model.ih"

void Model::train(const string &path, ostream *out)
{
    ifstream in(path);
    if (!in.good())
    {
        cerr << "Could not read training file: " << path << '\n';
        exit(1);
    }
    genLookup(&in, d_config->numTrain);
    in.close();
    in.open(path);
    ofstream featFile(d_config->featsPath);
    if (!featFile.good())
    {
        cerr << "Could not write features: " << d_config->featsPath << '\n';
        exit(1);
    }
    featFile << d_config->header;
    
    auto start_time = chrono::high_resolution_clock::now();
    size_t sentIdx = 0;
    size_t skipped = 0;
    size_t words = 0;
    while(++sentIdx)
    {
        if (d_skip.size() > sentIdx && d_skip[sentIdx])
        {
            ++skipped;
            readGold(&in);
            continue;
        }
        if(!readGold(&in))
        {
            //cerr << "not enough sentences\n";
            break;
        }
        d_cands.clear();
        d_csFeats.clear();
        d_featVals.clear();
        for (d_wordIdx = 0; d_wordIdx < d_origs.size(); ++d_wordIdx)
        {
            ++words;
            if (words ==  d_config->numTrain && d_config->numTrain != 0)
                break;
            gen(false);
            addGold();
            writeFeats(&featFile);
        }
        if (words ==  d_config->numTrain && d_config->numTrain != 0)
            break;
    }
    cerr << "Trained on " << sentIdx -skipped << " sentences\n";
    featFile.close();

    d_forest = loadForest(true, false);
    d_forest->run(true);
    d_forest->saveToFile();
    d_forest->writeOutput();
    delete d_forest;
    d_config->train = false;

    auto end_time = chrono::high_resolution_clock::now();
    d_eval.setTrainSecs(chrono::duration_cast<chrono::seconds>
                            (end_time - start_time).count());
}

