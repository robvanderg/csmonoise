#include "model.ih"

string Model::toFsa()
{
    string fsa = "";
    if (d_cands[d_wordIdx].empty())
        fsa += to_string(d_wordIdx) + ' ' + d_origs[d_wordIdx] + ' ' + 
                to_string(d_wordIdx+1) + " 1.0\n";
    else
    {
        size_t end = min(d_cands[d_wordIdx].size(), d_config->numCands);
        double total = 0.0;
        for (size_t iter = 0; iter != end; ++iter)
            total += d_results[d_wordIdx][iter];
        for (size_t candIdx = 0; candIdx != end; ++candIdx)
        {
            double val = d_results[d_wordIdx][candIdx] / total;
            fsa += to_string(d_wordIdx) + ' ' + d_cands[d_wordIdx][candIdx] + ' ' + 
                to_string(d_wordIdx + 1) + ' ' + to_string(val) + '\n';
        }
    }

    if (d_wordIdx == d_origs.size()-1)
        fsa += ".\n";
    return fsa;
}

