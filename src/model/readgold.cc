#include "model.ih"

bool Model::readGold(istream *input)
{
    d_origs.clear();
    d_oov.clear();
    d_cors.clear();
    string line;
    bool gotLine = false;
    while(getline((*input), line))
    {
        gotLine = true;
        vector<string> splitted;
        splitString(line, '\t', splitted);
        if (splitted.size() == 0)
            break;
        if (splitted.size() == 1)
            splitted.push_back("");
        for(size_t wordIdx = 2; wordIdx < splitted.size(); ++ wordIdx)
            splitted[1] += " " + splitted[wordIdx];
            
        strip(&splitted[0]);
        strip(&splitted[1]);
        if (!d_config->caps)
        {
            splitted[0] = toLower(splitted[0]);
            splitted[1] = toLower(splitted[1]);
        }
        if (splitted[0] == "" and splitted[1] == "")
            break;

        d_origs.push_back(splitted[0]);
        d_cors.push_back(splitted[1]);
    }
    return gotLine;
}

