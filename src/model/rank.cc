#include "model.ih"

void Model::rank()
{
    for (d_wordIdx = 0; d_wordIdx != d_origs.size(); ++d_wordIdx)
    {
        if (d_cands[d_wordIdx].empty())
            continue;
        d_forest->load(d_cands[d_wordIdx].size(), &d_featVals[d_wordIdx][0]);
        d_forest->run(true);
        d_results[d_wordIdx] = vector<double>(d_cands[d_wordIdx].size());
        d_forest->copyResults(&d_results[d_wordIdx][0]);

        sortCands();
    }
}

