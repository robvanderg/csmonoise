#include "model.ih"

void Model::genLookup(istream *in, size_t numSents)
{
    d_lookup = Lookup();
    for (size_t lineIdx = 0; (lineIdx != numSents || numSents == 0) && readGold(in); ++lineIdx)
    {
        if (d_skip.size() > 0 && d_skip[lineIdx])
            continue;
        for (size_t wordIdx = 0; wordIdx != d_origs.size(); ++wordIdx)
            d_lookup.addWord(d_origs[wordIdx], d_cors[wordIdx]);
    }

    d_lookup.optimize();
    d_lookup.save(d_config->lookupPath);
}
