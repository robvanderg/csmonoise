#include "model.ih"

vector<string> Model::tokenize(const string &source, const string &del)
{
    static bool dict[256] = { false};
    for (size_t i = 0; i < del.size(); ++i)
        dict[size_t(del[i])] = true;

    istringstream iss(source);
    vector<string> res;
    string token;
    while (iss >> token)
    {
        size_t punct = 0;
        size_t alpha = 0;
        for (char c: token)
        {
            if (dict[(size_t)c])
                ++punct;
            else
                ++alpha;
        }
        //if (alpha < 2)
        //{
        //    res.push_back(token);
        //    continue;
        //}
        
        if (dict[(size_t)token[0]])
        {
            // split punct from beginning 
            size_t split = 0;
            while (dict[(size_t)token[++split]]);
            string punct = token.substr(0, split);
            res.push_back(punct);
            token = token.substr(split, string::npos);
        }

        if (dict[(size_t)token.back()])
        {
            // split punct from end
            size_t split = token.size() - 1;
            while (dict[(size_t)token[--split]]);
            ++split;
            string punct = token.substr(split, string::npos);
            token = token.substr(0, split);
            res.push_back(token);
            res.push_back(punct);
        }
        else
            res.push_back(token);
    }
    return res;
}


