#include "model.ih"

void Model::interactive()
{
    d_forest = 0;
    vector<string> emptyVec;
    d_forest = new ForestProbability;
    d_forest->initCpp("gold", MEM_DOUBLE, d_config->header, // depvar, memMode, inputFile
                0, d_config->featsPath, 500, //mtry, outprefix, numTrees
                &cerr, d_config->seed, d_config->numThreads, // verboseOut, seed, nthreads, 
                d_config->regrPath, DEFAULT_IMPORTANCE_MODE, //forestPath, importance measure
                0, "", emptyVec, //targetpartitionsize?, splitweightsfile, alwayssplitvars
                "", true, emptyVec, // statusvarname, replace, catvars
                false, DEFAULT_SPLITRULE, "", //savemem, splitrule, caseweights
                false, 1, DEFAULT_ALPHA, DEFAULT_MINPROP, //predall, fraction, minprob
                false, DEFAULT_PREDICTIONTYPE); // holdout, predictiontype

    cerr << "loaded; just type your tweets:\n";
    string sent;
    while(getline(cin, sent))
    {
        if (sent.substr(0,4) == "setW")
        {
            istringstream iss(sent);
            string word;
            iss >> word >> word;
            try
            {
                d_config->weight = stod(word);
            }
            catch(...)
            {
                cout << "can not convert " << word << '\n' << '\n';
                continue;
            }
            cout << "set the weight to: " << d_config->weight << '\n' << '\n';
            continue;
        }
        if(sent == "")
            continue;
        string word;
        istringstream iss(sent);
        while(iss >> word)
            d_origs.push_back(word);

        d_cands.clear();
        d_featVals.clear();
        d_results.clear();
        for (d_wordIdx = 0; d_wordIdx < d_origs.size(); ++d_wordIdx)
            gen(false);
        rank();
        for (d_wordIdx = 0; d_wordIdx < d_origs.size(); ++d_wordIdx)
            write(&cout);
        d_origs.clear();
        cout << '\n';
    }
}

