#include "model.ih"

void Model::gen(bool errDet)
{
    vector<bool> d_copyIdxs(d_config->numFeats, false);
    size_t langId = 2;
    if (!d_config->extraFeat)
        langId = 3;
    else if (d_csFeats[d_wordIdx] == "tr" or d_csFeats[d_wordIdx] == "id")
        langId = 1;
    d_cands.push_back(vector<string>());
    d_featVals.push_back(vector<double>());
    d_results.push_back(vector<double>());
    d_curCands.clear();
    
    string curWord = d_origs[d_wordIdx];
    
    if (!consider())
        return;
    d_featIdx = 0;
    
    // binary feature which indicates if cand == origWord
    if (!d_config->goldErrDet)
    {
        addCands(&curWord, &d_config->idxs[1], 1, false);
        ++d_featIdx;//1
    }

    // word2vec 
    vector<string> candsW = vector<string>(40);
    vector<double> valsW = vector<double>(40);
    
    if (d_config->featGroups[0])
    {
        if (langId == 1 || langId == 3)
        {
            if (d_w2v.find(&curWord[0], &candsW[0], &valsW[0]) && !errDet)
            {
                addCands(&candsW[0], &valsW[0], 40, true);
                ++d_featIdx;
                addCands(&candsW[0], &d_config->idxs[1], 40, true);
                ++d_featIdx;
            }
            else
                d_featIdx += 2;
        }
        if (langId == 2  || langId == 3)
        {
            if (d_w2v2.find(&curWord[0], &candsW[0], &valsW[0]) && !errDet)
            {
                addCands(&candsW[0], &valsW[0], 40, true);
                ++d_featIdx;
                addCands(&candsW[0], &d_config->idxs[1], 40, true);
                ++d_featIdx;
            }
            else
                d_featIdx += 2;
        }
    }
    
    //aspell
    if (d_config->featGroups[1])
    {
        if (langId == 1 || langId == 3)
        {
            if (d_asp.find(curWord) && !errDet)
            {
                addCands(d_asp.getCands(), d_asp.getVals(), d_asp.getNumCands(), true);
                ++d_featIdx;
                addCands(d_asp.getCands(), &d_config->idxs[1], d_asp.getNumCands(), true);
                ++d_featIdx;
            }
            else
                d_featIdx += 2;
        }
        if (langId == 2 || langId == 3)
        {
            if (d_asp2.find(curWord) && !errDet)
            {
                addCands(d_asp2.getCands(), d_asp2.getVals(), d_asp2.getNumCands(), true);
                ++d_featIdx;
                addCands(d_asp2.getCands(), &d_config->idxs[1], d_asp2.getNumCands(), true);
                ++d_featIdx;
            }
            else
                d_featIdx += 2;
        }
    }

    //add words from lookup list
    if (d_config->featGroups[2])
    {
        size_t total = 0;
        for (auto const & cand: d_lookup.getLookups(curWord))
        {
            total += cand.second;
            if (errDet)
            {
                if (d_lookup.contains(curWord))
                    d_featVals[d_wordIdx][d_featIdx] = 1;
            }
            else
            {
                addCands(&cand.first, &cand.second, 1, true);
            }
        }
        ++d_featIdx;//6
    }

    // add candidates that fit the regexp "word.*"
    if (d_config->featGroups[3])
    {
        if (langId == 1 || langId == 3)
        {
            if (curWord.size() > 2 && !errDet)
            {
                pair<size_t, size_t> range = d_dict.getRange(&curWord[0]);
                if ((range.second - range.first) < 1000)//TODO, this is not so nice, hardcoded value similar as in addCands
                    for (size_t beg = range.first; beg != range.second; ++beg)
                    {
                        string cand(d_dict.getWord(beg));
                        addCands(&cand, &d_config->idxs[1], 1, true);
                    }
            }
            ++d_featIdx;//7
        }
        if (langId == 2 || langId == 3)
        {
            if (curWord.size() > 2 && !errDet)
            {
                pair<size_t, size_t> range = d_dict2.getRange(&curWord[0]);
                if ((range.second - range.first) < 1000)//TODO, this is not so nice, hardcoded value similar as in addCands
                    for (size_t beg = range.first; beg != range.second; ++beg)
                    {
                        string cand(d_dict2.getWord(beg));
                        addCands(&cand, &d_config->idxs[1], 1, true);
                    }
            }
            ++d_featIdx;//7
        }
    }
   
    // try to split word into 2 iv words
    if (d_config->featGroups[4] && !errDet)
    {
        if (langId ==  1 || langId == 3)
        {
            split(&d_dict);
            ++d_featIdx;//8
        }
        if (langId == 2 || langId == 3)
        {
            split(&d_dict2);
            ++d_featIdx;//8
        }
    }

    // Capitalization features
    if (d_config->featGroups[11])
    {
         string curWordCaps = curWord;
         transform(curWordCaps.begin(), curWordCaps.end(), curWordCaps.begin(), ::tolower);

         //add lowercased
         addCands(&curWordCaps, &d_config->idxs[1], 1, true);
         ++d_featIdx;

         // add with starting cap
         curWordCaps[0] = toupper(curWordCaps[0]);
         addCands(&curWordCaps, &d_config->idxs[1], 1, true);
         ++d_featIdx;
    }

    
    // Add feature from input file (ie. the same for all candidates for this word
    if (d_config->extraFeat)
    {
        for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
        {
            // convert lang1 to 1  lang2 to 2 and other to 3
            if (d_csFeats[d_wordIdx] == "tr" or d_csFeats[d_wordIdx] == "id")
                d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx] = 1.0;
            else if (d_csFeats[d_wordIdx] == "de" or d_csFeats[d_wordIdx] == "en")
                d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx+1] = 1.0;
            else // =="un"
                d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx+2] = 1.0;
        }
        d_featIdx += 3;
    }
 
    // ngram features
    if (d_config->featGroups[5])
    {
        if (langId == 1 || langId == 3)
        {
            fill (d_copyIdxs.begin()+d_featIdx, d_copyIdxs.begin() + d_featIdx + 6, true);
            addNgrams( &d_google);
            d_featIdx += 3;
            addNgrams( &d_twitter);
            d_featIdx += 3;
        }
        if (langId == 2 || langId == 3)
        {
            fill (d_copyIdxs.begin()+d_featIdx, d_copyIdxs.begin() + d_featIdx + 6, true);
            addNgrams( &d_google2);
            d_featIdx += 3;
            addNgrams( &d_twitter2);
            d_featIdx += 3;
        }
    }
    
    // binary dictionary lookup
    if (d_config->featGroups[6])
    {
        if (langId == 1 || langId == 3)
        {
            for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
                if (d_dict.contains(d_cands[d_wordIdx][candIdx]))
                    d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx] = 1.0;
            d_copyIdxs[d_featIdx] = true;
            ++d_featIdx;//15
        }
        if (langId == 2 || langId == 3)
        {
            for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
                if (d_dict2.contains(d_cands[d_wordIdx][candIdx]))
                    d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx] = 1.0;
            d_copyIdxs[d_featIdx] = true;
            ++d_featIdx;//15
        }
    }

    // checks character order
    if (d_config->featGroups[7] && !errDet)
    {
        for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
            if (match(curWord, d_cands[d_wordIdx][candIdx]))
                d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx] = 1.0;
        d_copyIdxs[d_featIdx] = true;
        ++d_featIdx;//16
    }

    // length features
    if (d_config->featGroups[8])
    {
        for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
            d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx] = d_cands[d_wordIdx][candIdx].size();
        d_copyIdxs[d_featIdx] = true;
        ++d_featIdx;//17
    }

    // Detect if word should be normalized in this corpus
    if (d_config->featGroups[9])
    {
        for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
            d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx] = isWord(d_cands[d_wordIdx][candIdx]);
        ++d_featIdx;//18
    }

    // add wordIdx as feature (mainly for capitalization
    if (d_config->featGroups[11])
    {
        for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
            d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx] = d_wordIdx;
        ++d_featIdx;
    }

    // complete w2v
    if (d_config->featGroups[0] && !errDet)
    {
        if (langId == 1 || langId == 3)
        {
            for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
                if (d_featVals[d_wordIdx][candIdx * d_config->numFeats + 1] == 0.0)
                    d_featVals[d_wordIdx][candIdx * d_config->numFeats + 1] = d_w2v.getDistance(curWord, d_cands[d_wordIdx][candIdx]);
        }
        if (langId == 2)
        {
            for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
                if (d_featVals[d_wordIdx][candIdx * d_config->numFeats + 1] == 0.0)
                    d_featVals[d_wordIdx][candIdx * d_config->numFeats + 1] = d_w2v2.getDistance(curWord, d_cands[d_wordIdx][candIdx]);
        }
        if (langId == 3)
        {
            for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
                if (d_featVals[d_wordIdx][candIdx * d_config->numFeats + 2] == 0.0)
                    d_featVals[d_wordIdx][candIdx * d_config->numFeats + 2] = d_w2v2.getDistance(curWord, d_cands[d_wordIdx][candIdx]);
        }
    }

    // singleFeats
    for (size_t beg = 0; beg != d_config->singleFeats.size(); ++beg)
        if (!d_config->singleFeats[beg])
            for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
                d_featVals[d_wordIdx][candIdx * d_config->numFeats + beg] = 0.0;

    //TODODODODO
    // add features of original word
    /*if (d_config->featGroups[10])
    {
        for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++ candIdx)
        {
            std::copy(&d_featVals[d_wordIdx][10], &d_featVals[d_wordIdx][19], &d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx]);
            //d_featVals[d_wordIdx][candIdx *d_config->numFeats] = d_featVals[d_wordIdx][5];
            //std::copy(&d_featVals[d_wordIdx][8], &d_featVals[d_wordIdx][17], &d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx + 1]);
        }
        d_featIdx+=9;
        TODO this doesnt work anymore...
    }*/
    //TODO lookup list is not included! [5]
    size_t realNumFeats = d_featIdx;

    if (d_config->featGroups[10])
    {
        for (size_t featIdx = 0; featIdx != realNumFeats; ++featIdx)
        {
            if (d_copyIdxs[featIdx])
            {
                //cout << featIdx << '\t' << d_featVals[d_wordIdx][featIdx] << '\n';
                for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++ candIdx)
                {
                    d_featVals[d_wordIdx][candIdx * d_config->numFeats + d_featIdx] = d_featVals[d_wordIdx][featIdx];
                }
                ++d_featIdx;
            }
        }
    }
}

