#include "model.ih"

void Model::addNgrams(NGram *langModel)
{
    for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
    {
        string cand = d_cands[d_wordIdx][candIdx];
        string prevWord = (d_wordIdx == 0)? "<S>": d_origs[d_wordIdx-1];
        string nextWord = (d_wordIdx == d_origs.size() - 1)? "</S>": 
                            d_origs[d_wordIdx+1];

        double curCandCount = double(langModel->getCount(cand) + 1);
        uint64_t prevCount = langModel->getCount(prevWord, cand);
        uint64_t nextCount = langModel->getCount(cand, nextWord);
       
        size_t idx = candIdx * d_config->numFeats + d_featIdx; 
        d_featVals[d_wordIdx][idx] = curCandCount / langModel->getTotalcount();
        d_featVals[d_wordIdx][idx + 1] = prevCount / curCandCount;
        d_featVals[d_wordIdx][idx + 2] = nextCount / curCandCount;
    }
}
