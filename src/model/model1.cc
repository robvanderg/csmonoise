#include "model.ih"

Model::Model(Config *config)
:
    d_config(config),
    d_w2v(config->w2v, config->w2v + ".cache", true),
    d_asp(config->aspLan, config->aspDict, config->aspMode),
    d_twitter(config->twitter, true),
    d_google(config->wiki, true),
    d_knowns(config->knowns, false),
    d_dict(config->dict, false), //TODO check!
    d_lookup(((config->train)?"":config->lookupPath)),
    d_eval(d_config->verbose, d_config->goldErrDet, d_config->goldErrDet2 && !d_config->sepErrDet),
    d_w2v2(config->w2v2, config->w2v2 + ".cache", true),
    d_asp2(config->aspLan2, config->aspDict2, config->aspMode),
    d_twitter2(config->twitter2, true),
    d_google2(config->wiki2, true),
    d_dict2(config->dict2, false)
{
    //Run a dummy to find number of features
    // This is done so that the memory can be reserved efficiently
    d_origs.push_back("dummy");
    d_wordIdx = 0;
    if (d_config->extraFeat)
        d_csFeats.push_back("other");
    gen(false);
    d_config->numFeats = d_featIdx + 1;
    d_config->header = "";
    for (size_t beg =0; beg != d_config->numFeats-1; ++beg)
        d_config->header += "feat" + std::to_string(beg) + "\t";
    d_config->header += "gold\n";

    d_origs.clear();
    d_cands.clear();
    d_featVals.clear();
    d_csFeats.clear();
    d_results.clear();
}

