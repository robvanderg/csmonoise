#include "model.ih"

void Model::write(ostream *out)
{
    for (d_wordIdx = 0; d_wordIdx < d_origs.size(); ++d_wordIdx)
    {
        if (d_config->numCands == 0)
        {
            (*out) << d_origs[d_wordIdx] << '\t' << ((d_cands[d_wordIdx].empty())? d_origs[d_wordIdx]: d_cands[d_wordIdx][0])
                << '\n';
            if (d_wordIdx == d_origs.size()-1)
                (*out) << '\n';

        }
        else
            (*out) << toFsa();
    }
}

