#include "model.ih"

void Model::errDet()
{
    d_cands.clear();
    d_featVals.clear();
    d_results.clear();
    for (d_wordIdx = 0; d_wordIdx < d_origs.size(); ++d_wordIdx)
    {
        d_oov[d_wordIdx] = true;
        gen(true);
        if (d_cands[d_wordIdx].empty())
            continue;
        d_forestDet->load(d_cands[d_wordIdx].size(), &d_featVals[d_wordIdx][0]);
        d_forestDet->run(true);
        d_results[d_wordIdx] = vector<double>(d_cands[d_wordIdx].size());
        d_forestDet->copyResults(&d_results[d_wordIdx][0]);
        if (d_results[d_wordIdx][0] <= d_config->errDetWeight)
            d_oov[d_wordIdx] = false;
    }
}
