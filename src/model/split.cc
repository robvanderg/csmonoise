#include "model.ih"

void Model::split(Vocab *dict)
{
    string origWord = d_origs[d_wordIdx];
    if (origWord.size() < 3)
        return;
    for (size_t beg = 1; beg != origWord.size()-1; ++beg)
    {
        string word1 = origWord.substr(0, beg);
        string word2 = origWord.substr(beg);
        if (dict->contains(word1) && dict->contains(word2))
        {
            string cand = word1 + " " + word2;
            addCands(&cand, &d_config->idxs[1],1,true);
        }
    }
}
