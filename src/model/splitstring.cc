#include "model.ih"

void Model::splitString(const string &s, char delim, vector<string> &elems){

    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim))
        elems.push_back(item); 
}


