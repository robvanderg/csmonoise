#include "model.ih"

void Model::writeFeats(ofstream *ofs)
{
    for (size_t candIdx = 0; candIdx != d_cands[d_wordIdx].size(); ++candIdx)
    {
        for (size_t featIdx = 0; featIdx != d_config->numFeats; ++featIdx)
            (*ofs) << d_featVals[d_wordIdx][candIdx * d_config->numFeats + featIdx] << '\t';
        (*ofs) << '\n';
    }
}

