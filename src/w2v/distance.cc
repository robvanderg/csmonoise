#include "w2v.ih"

double w2v::distance(string const &word1, string const &word2)
{
    int idx1 = getId(word1);
    if (idx1 == 0)
        return 0.0;
    int idx2 = getId(word2);
    if (idx2 == 0)
        return 0.0;
    dist = 0;
    for (a = 0; a < size; a++) {
        dist += M[a + idx1 * size] * M[a + idx2 * size];
    }
    return dist;
}
