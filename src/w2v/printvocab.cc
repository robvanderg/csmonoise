#include "w2v.ih"

void w2v::printVocab()
{
    for (int beg = 0; beg != words; ++beg)
        cout << beg << ".\t" << &vocab[beg * max_w] << '\n';
}
