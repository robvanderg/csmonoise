# Lexical normalization for code-switched data

This repository contains an adaptation of
[MoNoise](http://bitbucket.org/robvanderg/monoise) to handle code-switched
data. It performs the task of lexical normalization for utterances with multiple languages:

```
ak .  luv  u   ;( till die
aku . love you ;( till die
```


Usage is similar to the original MoNoise; however it has two additional arguments:

```
-2 path to 2nd language to model, usage similar as -d
-E Read an the language label from the input, and use this to split the features (langaware)
```

For all annotation layers of the dataset, we refer to https://github.com/ozlemcek/TrDeNormData

## Folders in this repo:
* bilty: data in the correct format for BiLSTM tagger
* config: configuration files for BERT tagger
* data: contains all alternations of the data for both language pairs
* posPreds: predictions for pos experiments
* preds: contains predictions of the normalization model
* scripts: all scripts for reproducing the experiments (see scripts/runAll.sh)
* src: the source code of the code-switch version of MoNoise

For the experiments, the following additional folders are used:
* bilst-aux: BiLSTM tagger
* monoise: original (mono-lingual) version of MoNoise
* mtp: BERT tagger
* MUSE: to merge two mono-lingual embeddings to the same space
* working: to save the normalization models

For now all files containing the test data are left out. Contact one of the authors if you have interest/questions about this.

## More information about the models
For more information about the different models, we refer to the paper; to use the models described in the paper:

* monolingual: is just the basic version of MoNoise
* fragment-based: also uses the basic version of MoNoise, but splits the data first
* multilingual: only specify -2
* language-aware: specify -2 and use -E


## Reproducability
You can run `./scripts/runAll.sh` to reproduce all experiments. However, we
would suggest to run the commands separately and parallelize them to save time.
All predictions are also included in this repository. `scripts/genAll.sh` can 
be used to generate all tables from the paper. It should be noted that the 
test split is temporarily not available. Contact one of the authors for more 
information on this.

## Acknowledgements
We gratefuly made use of the following projects:

* [MoNoise](http://bitbucket.org/robvanderg/monoise)
* [MarMot](http://cistern.cis.lmu.de/marmot/)
* [Bilty](https://github.com/bplank/bilstm-aux)
* [MaChAmp](https://machamp-nlp.github.io)
* [MUSE](https://github.com/facebookresearch/MUSE)

## Citation
```
@inproceedings{csnorm2021,
    title = "Lexical Normalization for Code-switched Data and its Effect on POS Tagging",
    author = "van der Goot, Rob and \c{C}etino{\u{g}}lu, \"O}zlem",
    booktitle = "Proceedings of the 16th Conference of the {E}uropean Chapter of the Association for Computational Linguistics: Volume 1, Long Papers",
    year = "2021",
    publisher = "Association for Computational Linguistics",
}
```
